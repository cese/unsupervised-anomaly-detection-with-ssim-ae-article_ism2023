import matplotlib.pyplot as plt

# cSpell: ignore skimage, matplotlib, labelsize, pyplot, plt, fontsize, titlesize, xtick, ytick
# cSpell: ignore Jaccard Sørensen, MVTEC, ssim

import math
import sys
import os
import os.path
import cv2
from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
from tqdm import tqdm
import argparse

from AE_training import architecture_MVTEC
from typing import NamedTuple

import properties as dp
import metrics as m

plt.rc('font', size=12)
plt.rc('xtick', labelsize=12)    # font size of the tick labels
plt.rc('ytick', labelsize=12)    # font size of the tick labels
plt.rc('legend', fontsize=12)    # legend font size
plt.rc('figure', titlesize=12)   # font size of the figure title


def calculate_TP_TN_FP_FN(ground_truth, predicted_mask):
    # print(f"Matrix: ground_truth.shape:{ground_truth.shape}, predicted_mask:{predicted_mask.shape}")
    # TODO: precalculate (ground_truth !=/== predicted_mask
    TP = np.sum(np.multiply((ground_truth == predicted_mask), predicted_mask == 1))
    TN = np.sum(np.multiply((ground_truth == predicted_mask), predicted_mask == 0))
    FP = np.sum(np.multiply((ground_truth != predicted_mask), predicted_mask == 1))
    FN = np.sum(np.multiply((ground_truth != predicted_mask), predicted_mask == 0))
    return TP, TN, FP, FN


def AP(TP, TN, FP, FN):
    return (TP+TN)/(TP+TN+FP+FN)


def DICE(TP, TN, FP, FN):
    """See: https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient\n
    Same as: https://en.wikipedia.org/wiki/F-score\n
    Similar to: https://en.wikipedia.org/wiki/Jaccard_index for binary case\n
    Let J be the Jaccard index and S the Sørensen–Dice coefficient, then we have:\n
    J = S/(2-S)\n
    S = 2J(1+J)\n
    The Sørensen–Dice coefficient is not a metric.\n
    Its range is from 0 to 1 (like the Jaccard index).\n
    The difference 1 - S is also not a metric.1n
    Other versions: logDICE\n
    Used for image segmentation. Measures intersection of 2 fuzzy sets.\n
    In this code, it is the same as the Jaccard index because it s applied to th binary case.\n
    """
    if (2*TP+FP+FN) == 0:
        # TODO: should this not return None
        # TODO: if FP == FN == 0, then we can return a 1
        # TODO: if TP == 0, we can return 0
        return 1
    else:
        return (2*TP)/(2*TP+FP+FN)


def FPR(TP, TN, FP, FN):
    if FP+TN == 0:
        return 0.0
    else:
        return FP/(FP+TN)


def TPR(TP, TN, FP, FN):
    if TP+FN == 0:
        return 1.0
    else:
        return TP/(TP+FN)


def Youden_statistic(TP, TN, FP, FN):
    """see https://en.wikipedia.org/wiki/Youden%27s_J_statistic"""
    if (TP+FN) == 0 or (TN+FP) == 0:
        return None
    else:
        return TP/(TP+FN) + TN/(TN+FP) - 1


def calculate_AUC(ROC_curve):
    AUC = 0
    FPR_values = [x[0] for x in ROC_curve]
    TPR_values = [x[1] for x in ROC_curve]
    for i in range(0, len(FPR_values)-1):
        a = TPR_values[i]
        b = TPR_values[i+1]
        h = FPR_values[i+1]-FPR_values[i]
        AUC += (a+b)*h/2
    return AUC


def plot_ROC_curve(datasets, results, results_dir):
    plt.clf()
    for metric_results in results:
        FPR_values = [x[1] for x in metric_results["FPR"]]
        TPR_values = [x[1] for x in metric_results["TPR"]]
        plt.plot(FPR_values, TPR_values, "-o")
    plt.xlim(0, 1)
    plt.xlabel("FPR", fontsize=12)
    plt.ylabel("TPR")
    plt.title("ROC Curve")
    plt.legend(datasets)
    plt.grid()
    plt.savefig(os.path.join(results_dir, "ROC_curve.png"))


def plot_YoundStat_thresh(datasets, results, results_dir):
    plt.clf()
    i = 0
    for metric_results in results:
        YoundStat_values = [x[1] for x in metric_results["YoundenStat"]]
        thresh_values = [x[0] for x in metric_results["YoundenStat"]]
        plt.plot(thresh_values, YoundStat_values, "-o", label=datasets[i])
        younden_thresh, younden_max = max(metric_results["YoundenStat"], key=lambda x: x[1])
        plt.vlines(x=younden_thresh, ymin=0, ymax=younden_max, ls='--', colors='red', label=f"({younden_thresh:.2f}, {younden_max:.2f})")
        i = i + 1
    plt.xlabel("Threshold")
    plt.ylabel("Younden's Statistic")
    plt.title("Younden's Statistic (Threshold)")
    # plt.legend(datasets)
    plt.legend(loc='best', fontsize='small', title_fontsize='small')
    plt.grid()
    plt.savefig(os.path.join(results_dir, "YoundStat_thresh.png"))


def plot_DICE_thresh(datasets, results, results_dir):
    plt.clf()
    i = 0
    for metric_results in results:
        DICE_values = [x[1] for x in metric_results["DICE"]]
        thresh_values = [x[0] for x in metric_results["DICE"]]
        plt.plot(thresh_values, DICE_values, "-o", label=datasets[i])
        dice_thresh, dice_max = max(metric_results["DICE"], key=lambda x: x[1])
        plt.vlines(x=dice_thresh, ymin=0, ymax=dice_max, ls='--', colors='red', label=f"({dice_thresh:.2f}, {dice_max:.2f})")
        i = i + 1
    plt.xlabel("Threshold")
    plt.ylabel("DICE")
    plt.title("DICE (Threshold)")
    # plt.legend(datasets)
    plt.legend(loc='best', fontsize='small', title_fontsize='small')
    plt.grid()
    plt.savefig(os.path.join(results_dir, "DICE_thresh.png"))


class Sample(NamedTuple):
    dataset: str
    category: str
    predicted_file_name: str
    img_orig: np.ndarray
    original_file_name: str
    img_predict: np.ndarray
    ground_truth: np.ndarray
    truth_file_name: str
    diff: np.ndarray
    loss_file_name: str


def read_data(dataset):

    (model_id,dataset_name) = get_model_id_and_name(dataset)
    print(f"read_data : model_id = {model_id}")
    print(f"read_data : dataset_name = {dataset_name}")
    base_dir = os.path.join(dp.Properties.results_dir, dataset_name, model_id)
    print(f"read_data : base_dir = {base_dir}")

    samples = []
    img_orig_samples = []
    img_predict_samples = []
    ground_truth_samples = []

    test_files = f"{dp.Properties.data_cache}/{dataset_name}/test/"
    print(f"test_files = {test_files}")
    # for category in tqdm(os.listdir(f"{dp.Properties.data_cache}/{dataset}/test/")):
    for category in tqdm(os.listdir(test_files)):
        # We need these for metric calculations (evaluation and threshold selection)
        # if category == "good":
        #     continue
        for img_name in os.listdir(f"{dp.Properties.data_cache}/{dataset_name}/test/{category}/"):

            # predicted_file_name = f"{dp.Properties.results_dir}/{dataset_name}/predicted/{category}/{img_name}"
            predicted_file_name = f"{base_dir}/predicted/{category}/{img_name}"
            print(f"Loading predicted_file_name = {predicted_file_name}")
            img_predict = cv2.imread(predicted_file_name, cv2.IMREAD_GRAYSCALE)

            original_file_name = f"{dp.Properties.data_cache}/{dataset_name}/test/{category}/{img_name}"
            print(f"Loading original_file_name = {original_file_name}")
            img_orig = cv2.imread(original_file_name, cv2.IMREAD_GRAYSCALE)

            if category != "good":
                # We have a mask, so us it
                if dataset in ["texture_1", "texture_2"]:
                    truth_file_name = f"{dp.Properties.data_cache}/{dataset_name}/ground_truth/{category}/{img_name}"
                    print(f"Loading truth_file_name = {truth_file_name}")
                    ground_truth = cv2.imread(truth_file_name, cv2.IMREAD_GRAYSCALE)
                else:
                    truth_file_name = f"{dp.Properties.data_cache}/{dataset_name}/ground_truth/{category}/{img_name[:-4]}_mask.png"
                    print(f"Loading truth_file_name = {truth_file_name}")
                    ground_truth = cv2.imread(truth_file_name, cv2.IMREAD_GRAYSCALE)
            else:
                # no mask, so create a mask of zeros indicating no anomalies
                truth_file_name="--good--"
                ground_truth = np.zeros(shape=img_orig.shape, dtype=np.uint8)

            # print(f"type(ground_truth) = {type(ground_truth)}")
            # print(f"len(ground_truth) = {len(ground_truth)}")
            # print(f"ground_truth.shape = {ground_truth.shape}")
            # print(f"type(ground_truth[0][0]) = {type(ground_truth[0][0])}")
            # Make sure mask is binary
            ground_truth = (ground_truth > 0).astype(int)

            img_orig_samples.append(img_orig)
            img_predict_samples.append(img_predict)
            ground_truth_samples.append(ground_truth)

            # path = f"{dp.Properties.results_dir}/{dataset_name}/loss/{category}/{img_name}"
            path = f"{base_dir}/loss/{category}/{img_name}"
            if os.path.isfile(path):
                loss_file_name=path
            else:
                loss_file_name=None

            sample = Sample(dataset=dataset_name,
                            category=category, 
                            predicted_file_name=predicted_file_name,
                            img_orig=img_orig,
                            img_predict=img_predict,
                            original_file_name=original_file_name,
                            ground_truth=ground_truth,
                            truth_file_name=truth_file_name,
                            diff=None,
                            loss_file_name=loss_file_name)
            samples.append(sample)

    print(f"len(img_orig_samples) = {len(img_orig_samples)}")
    print(f"len(img_predict_samples) = {len(img_predict_samples)}")
    print(f"len(ground_truth_samples) = {len(ground_truth_samples)}")
    return samples, img_orig_samples, img_predict_samples, ground_truth_samples

def calculate_diff(samples: list[Sample], win_size=11):
    """Structural dissimilarity
    Usually applied to the luma only. "The resultant SSIM index is a 
    decimal value between -1 and 1, where 1 indicates perfect similarity, 
    0 indicates no similarity, and -1 indicates perfect anti-correlation."
    see https://en.wikipedia.org/wiki/Structural_similarity
    """

    loss_max=sys.float_info.min
    loss_min=sys.float_info.max
    loss_samples = []
    n = len(img_orig_samples)
    for i in range(0, n):
        img_orig = samples[i].img_orig.astype("float32") / 255.0
        img_predict = samples[i].img_predict.astype("float32") / 255.0
        # TODO: the range is not 1. Should we correct it?
        # max_out = img_predict.max()
        # min_out = img_predict.min()
        # data_range = max_out - min_out
        # if data_range != 1: raise RuntimeError(f"data_range = {data_range}")
        _, S = ssim( im1=img_orig, im2=img_predict, data_range=1, gradient=False, full=True, multichannel=False, win_size=win_size)
        # Structural dissimilarity (both similarity and dissimilarity are not a distance)
        # loss = 1/2 - S/2
        loss = (1 - S)/2
        loss_max = max(loss_max, np.max(loss))
        loss_min = min(loss_min, np.min(loss))
        updated_sample = samples[i]._replace(diff=loss)
        loss_samples.append(updated_sample)

    return loss_samples, loss_max, loss_min



def csv_header() -> str:
    return f"dataset, category, original_file_name, predicted_file_name, truth_file_name, loss_file_name, thresh, TP, TN, FP, FN, TPR, FPR\n"

def csv_out(s: Sample, thresh: float, TP: float, TN: float, FP: float, FN: float, TPR: float, FPR: float) -> str:
    return f"\"{s.dataset}\", \"{s.category}\", \"{s.original_file_name}\", \"{s.predicted_file_name}\", \"{s.truth_file_name}\", \"{s.loss_file_name}\", {thresh}, {TP}, {TN}, {FP}, {FN}, {TPR}, {FPR}\n"

def calculate_metrics(
        # dataset_name, 
        dataset, 
        samples: list[Sample],
        thresh_max=1
        ):

    (model_id,dataset_name) = get_model_id_and_name(dataset)
    print(f"calculate_metrics : model_id = {model_id}")
    print(f"calculate_metrics : dataset_name = {dataset_name}")

    metric_results = dict()
    metric_results["DICE"] = []
    metric_results["YoundenStat"] = []
    metric_results["TPR"] = []
    metric_results["FPR"] = []

    thresh_min = 0
    # TODO: this does not seem to be correct
    # thresh_values = np.linspace(thresh_min, thresh_max, num=100)**2
    thresh_values = np.linspace(thresh_min, thresh_max, num=100)
    # print(f"thresh_values (1): {thresZh_values}")
    # TODO: better plots
    thresh_values = thresh_values**2
    # print(f"thresh_values (2): {thresh_values}")
    thresh_values = [x for x in thresh_values]
    
    out_folder = f"{dp.Properties.results_dir}/{dataset_name}/{model_id}"
    os.makedirs(out_folder, exist_ok=True)
    print(f"Created/checked folder: {out_folder}")

    for thresh in thresh_values:

        metrics_name = f"metrics_{dataset_name}_{thresh}.csv"
        metrics_file = os.path.join(out_folder, metrics_name)
        print(f"Model {dataset_name} testing threshold: {thresh} @ {metrics_file}")
        with open(metrics_file, 'w') as f:
            f.write(csv_header() )
            # Dice_values = []
            # YoundenStat_values = []
            TPs = 0
            TNs = 0
            FPs = 0
            FNs = 0
            TPR_values = []
            FPR_values = []
            n = len(samples)
            # For each sample image, count the TP, TN, FP, FN, TPR, FPR per pixel  
            for i in range(0, n):
                loss_samples = samples[i].diff
                predicted_mask = (loss_samples > thresh).astype(int)
                ground_truth = samples[i].ground_truth
                TP, TN, FP, FN = calculate_TP_TN_FP_FN(ground_truth=ground_truth, predicted_mask=predicted_mask)
                TPs = TPs + TP
                TNs = TNs + TN
                FPs = FPs + FP
                FNs = FNs + FN
                # Dice_values.append(DICE(TP, TN, FP, FN))
                # youden_stats = Youden_statistic(TP, TN, FP, FN)
                # if youden_stats == None:
                #     print(f"confusion matrix: TP:{TP}, TN:{TN}, FP:{FP}, FN:{FN}")
                #     print(f"youden_stats: {youden_stats}")
                # YoundenStat_values.append(youden_stats)
                # An image can have no anomalies. Assume that for this image the 
                # threshold produces the correct result. So TP = 0 because no 
                # anomalies exist. The FN = 0 because all pixels are correctly 
                # marked as non-anomalies. So TPR = TP/(TP+FN) = 0/0 = NaN 
                tpr = TPR(TP, TN, FP, FN)
                TPR_values.append(tpr)
                # if math.isnan(tpr):
                #     print(f"TPR NaN: TP={FP}, TN={TN}, FP={FP}, FN={FN}")
                # else:
                #     TPR_values.append(tpr)
                # For FPR to be NaN, is highly improbable. 
                # TN = 0 means all pixels are anomalies (no negatives). Unlikely  
                # FPR = 0 means no good pixels are marked as anomalies (possible) 
                fpr = FPR(TP, TN, FP, FN)
                FPR_values.append(fpr)
                # if math.isnan(fpr):
                #     print(f"FPR NaN: TP={FP}, TN={TN}, FP={FP}, FN={FN}")
                # else:
                #     FPR_values.append(fpr)
                f.write(csv_out(samples[i],thresh, TP, TN, FP, FN, tpr, fpr) )
        # Calculate the metrics using the TP, TN, FP, FN values of all images
        dice_index = DICE(TPs, TNs, FPs, FNs)
        youden_index = Youden_statistic(TPs, TNs, FPs, FNs)
        # metric_results["DICE"].append((thresh, np.mean(Dice_values)))
        # metric_results["YoundenStat"].append((thresh, np.mean(YoundenStat_values)))
        metric_results["DICE"].append((thresh, dice_index))
        metric_results["YoundenStat"].append((thresh, youden_index))
        # Note: lists are converted to an array before numpy executes calculation
        metric_results["TPR"].append((thresh, np.mean(TPR_values)))
        metric_results["FPR"].append((thresh, np.mean(FPR_values)))

    f.close()
    print(f'Threshold metrics saved to {metrics_file}')

    DICE_max_thresh, DICE_max = max(metric_results["DICE"], key=lambda x: x[1])
    YoundenStat_max_thresh, YoundenStat_max = max(metric_results["YoundenStat"], key=lambda x: x[1])
    ROC_curve = list(zip([x[1] for x in metric_results["FPR"]], [x[1] for x in metric_results["TPR"]]))
    ROC_curve = sorted(ROC_curve, key=lambda x: x[0])
    AUC = calculate_AUC(ROC_curve)
    metrics_data = m.Metrics(dataset_name, AUC, YoundenStat_max, YoundenStat_max_thresh, DICE_max, DICE_max_thresh, metric_results)

    return metrics_data

def get_model_id_and_name(dataset_name: str) -> tuple[str, str] :
    # Remove first element of the path
    model_id = dataset_name.split(os.sep)
    dataset_name = model_id.pop(0)
    if len(model_id) > 0:
        model_id = os.path.join(*model_id)
    else:
        model_id = ""
    return (model_id, dataset_name)


# f"model_weights/{dataset_name}/"
# f"log/{dataset_name}/{model_id}"
def create_predicted(path_to_load_model = "model_weights", dataset_name="carpet", latent_dim=100, training_loss="ssim", batch_size=8):

    autoencoder = architecture_MVTEC(input_shape=(128, 128, 1), latent_dim=latent_dim)
    # path_to_load_model = f"{path_to_load_model}/{dataset_name}/"
    path_to_load_model = os.path.join(path_to_load_model, dataset_name)
    print(f"path_to_load_model = {path_to_load_model}")
    # Remove first element of the path
    model_id = dataset_name.split(os.sep)
    dataset_name = model_id.pop(0)
    if len(model_id) > 0:
        model_id = os.path.join(*model_id)
    else:
        model_id = ""
    print(f"model_id = {model_id}")
    print(f"dataset_name = {dataset_name}")
    name = f"a_{latent_dim}_loss_{training_loss}_batch_{batch_size}.hdf5"
    # path_to_load_model += name
    path_to_load_model = os.path.join(path_to_load_model, name)
    print(f"path_to_load_model file = {path_to_load_model}")
    autoencoder.load_weights(path_to_load_model)

    if dataset_name in ["texture_1", "texture_2"]:
        img_in_size = (512, 512)
        img_resized_size = (256, 256)
        crop_size = (128, 128)
        step = 16
    elif dataset_name in ["carpet", "grid"]:
        img_in_size = (1024, 1024)
        img_resized_size = (512, 512)
        crop_size = (128, 128)
        step = 32

    # for category in tqdm(os.listdir(f"data/{dataset_name}/test/")):
    for category in tqdm(os.listdir(f"{dp.Properties.data_cache}/{dataset_name}/test/")):

        base_dir = os.path.join(dp.Properties.results_dir, dataset_name, model_id)
        print(f"base_dir = {base_dir}")
        try:
            # os.makedirs(f"results/{dataset_name}/predicted/{category}/")
            # os.makedirs(f"{dp.Properties.results_dir}/{dataset_name}/predicted/{category}/")
            predicted = os.path.join(base_dir, "predicted", category)
            os.makedirs(predicted)
        except:
            pass

        try:
            # os.makedirs(f"results/{dataset_name}/loss/{category}/")
            # os.makedirs(f"{dp.Properties.results_dir}/{dataset_name}/loss/{category}/")
            loss = os.path.join(base_dir, "loss", category)
            os.makedirs(loss)
        except:
            pass

        # for img_name in tqdm(os.listdir(f"data/{dataset_name}/test/{category}/")):
        for img_name in tqdm(os.listdir(f"{dp.Properties.data_cache}/{dataset_name}/test/{category}/")):

            # img_in = cv2.imread(f"data/{dataset_name}/test/{category}/{img_name}", 0)
            file_i = f"{dp.Properties.data_cache}/{dataset_name}/test/{category}/{img_name}"
            print(f"Reading test image: {file_i}")
            img_in = cv2.imread(file_i, 0)
            img_in = img_in.astype("float32") / 255.0
            img_resized = cv2.resize(img_in, img_resized_size)
            img_out = np.zeros(shape=img_resized.shape)
            overlap = np.zeros(shape=img_resized.shape)

            for x in range(0, img_resized.shape[0]-crop_size[0]+1, step):
                for y in range(0, img_resized.shape[1]-crop_size[1]+1, step):

                    x_start = x
                    x_end = x_start + crop_size[0]
                    y_start = y
                    y_end = y + crop_size[1]
                    crop = img_resized[x_start:x_end, y_start:y_end]

                    X_test = []
                    X_test.append(crop)
                    X_test = np.array(X_test)
                    X_test = np.expand_dims(X_test, axis=-1)
                    img_predict = autoencoder.predict(X_test)

                    img_out[(x_start+1):(x_end-1), (y_start+1):(y_end-1)] += img_predict[0, 1:-1, 1:-1, 0]
                    overlap[(x_start+1):(x_end-1), (y_start+1):(y_end-1)] += np.ones(shape=crop_size)[1:-1, 1:-1]

            # where no overlap, the value is zero. Make sure it is 1
            overlap = np.where(overlap == 0, 1, overlap)
            # Calculate the mean based on the number of overlaps
            img_out = img_out/overlap
            img_out = cv2.resize(img_out, img_in_size)

            # cv2.imwrite(f"results/{dataset_name}/predicted/{category}/{img_name}", img_out*255)
            # cv2.imwrite(f"{dp.Properties.results_dir}/{dataset_name}/predicted/{category}/{img_name}", img_out*255)
            save = os.path.join(predicted, img_name)
            print(f"Saving reconstructon to: {save}")
            cv2.imwrite(save, img_out*255)

            # out = img_out[1:-1, 1:-1]
            # max_out = out.max()
            # min_out = out.min()
            # data_range = max_out - min_out
            data_range = 1
            _, _, S = ssim(img_in[1:-1, 1:-1], img_out[1:-1, 1:-1], 
                           gradient=True, full=True, multichannel=False, 
                           data_range=data_range
                           )

            plt.clf()
            # https://stackoverflow.com/questions/3453188/matplotlib-display-plot-on-a-remote-machine
            # https://discourse.julialang.org/t/plotting-while-working-with-vs-code-remote-ssh/34309
            plt.imshow(1-S, vmax=1, cmap="jet")
            plt.colorbar()

            # plt.savefig(f"results/{dataset_name}/loss/{category}/{img_name}")
            # plt.savefig(f"{dp.Properties.results_dir}/{dataset_name}/loss/{category}/{img_name}")
            save = os.path.join(loss, img_name)
            print(f"Saving heatmap to: {save}")
            plt.savefig(save)

def parse_args():
    parser = argparse.ArgumentParser('AE_SSIM')
    parser.add_argument('--metrics_file', type=str,  help='Append this to name of the metrics file', default="")
    parser.add_argument('--datasets_names', type=str, nargs='+',  help='List of paths to the models', default=["grid", "texture_1", "texture_2", "carpet"])
    parser.add_argument('--path_to_load_model', type=str,  help='Root for list of paths', default="model_weights")
    return parser.parse_args()

if __name__ == "__main__":

    args = parse_args()
    print(args)

    # Original setup with no model ID
    # datasets_names = ["grid", "texture_1", "texture_2", "carpet"]
    # datasets_names = ["grid", "carpet"]
    # datasets_names = ["grid"]
    # datasets_names = ["carpet"]
    # Regression testing (includes model id)
    # datasets_names = ["carpet/original", "carpet/regression_2b"]
    # datasets_names = ["grid/original", "grid/regression_2b"]
    # datasets_names = ["texture_1/original", "texture_1/regression_2b"]
    # datasets_names = ["texture_2/original", "texture_2/regression_2b"]
    results = []
    metrics = []
    print(f"Base folder: {args.path_to_load_model}")
    print(f"Dataset-names: {args.datasets_names}")
    metrics_file = f"metrics_{args.metrics_file}.txt"
    print(f"metrics_file: {metrics_file}")

    datasets_name = list(map(lambda d: get_model_id_and_name(d)[1], args.datasets_names))
    #print(f"Plots : model_ids = {model_ids}")
    print(f"Plots : datasets_name = {datasets_name}")

    metrics_file = os.path.join(dp.Properties.results_dir, metrics_file)
    with open(metrics_file, 'w') as f:

        for dataset_name in args.datasets_names:

            # 1) Create predicted images
            # Comment this out to save time when experimenting with evaluations after predictions are made
            # Original (path_to_load_model="model_weights")
            # create_predicted(dataset_name=dataset_name)
            # Regression testing
            # create_predicted(path_to_load_model="log", dataset_name=dataset_name)
            # Comment if you want to regenerate the images
            # create_predicted(path_to_load_model=args.path_to_load_model, dataset_name=dataset_name)

            # 2) Read original, predcited images and ground truth
            samples, img_orig_samples, img_predict_samples, ground_truth_samples = read_data(dataset=dataset_name)
            assert len(samples) == len(img_orig_samples)

            # 3) Calculate structural dissimilarity
            loss_samples, thresh_max, thresh_minx = calculate_diff(
                                    samples, 
                                    win_size=11
                                )
            # thresh_max = np.max(np.array(loss_samples))

            # 4) Calculate metrics
            metrics_datax = calculate_metrics(
                                                dataset=dataset_name,
                                                samples=loss_samples,
                                                thresh_max=thresh_max
                                            )
            print(metrics_datax.to_string())
            f.write(metrics_datax.to_string())

            results.append(metrics_datax.metric_results)



    f.close()
    print(f'Metrics saved to {metrics_file}')

    # (model_id,dataset_name) = get_model_id_and_name(dataset)
    datasets_name = list(map(lambda d: get_model_id_and_name(d)[1], args.datasets_names))
    #print(f"Plots : model_ids = {model_ids}")
    print(f"Plots : datasets_name = {datasets_name}")

    # plot_ROC_curve(datasets=args.datasets_names, results=results, results_dir=dp.Properties.results_dir)
    # plot_YoundStat_thresh(datasets=args.datasets_names, results=results, results_dir=dp.Properties.results_dir)
    # plot_DICE_thresh(datasets=args.datasets_names, results=results, results_dir=dp.Properties.results_dir)
    plot_ROC_curve(datasets=datasets_name, results=results, results_dir=dp.Properties.results_dir)
    plot_YoundStat_thresh(datasets=datasets_name, results=results, results_dir=dp.Properties.results_dir)
    plot_DICE_thresh(datasets=datasets_name, results=results, results_dir=dp.Properties.results_dir)
