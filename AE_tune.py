# cSpell: ignore tensorboard, keras, tf, sklearn, autoencoder
# cSpell: ignore Jaccard Sørensen, MVTEC, hyperparameters, hypermodel 

import math
import sys
import os
import os.path
from datetime import datetime
import cv2
from skimage.metrics import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
from tqdm import tqdm
from typing import NamedTuple

# import random
# import warnings
# import argparse
import tensorflow as tf
from tensorflow import keras
from keras import layers, models
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.optimizers import Adam
from keras.models import Model
from keras.layers import Input, Conv2D, Conv2DTranspose, LeakyReLU

import keras_tuner as kt
from tensorflow import keras

import argparse
from sklearn.model_selection import train_test_split

import properties as dp
import AE_training as train
import tfpipes as tfp


def example_build_model(hp):
  model = keras.Sequential()
  model.add(keras.layers.Dense(
      hp.Choice('units', [8, 16, 32]),
      activation='relu'))
  model.add(keras.layers.Dense(1, activation='relu'))
  model.compile(loss='mse')
  return model

def example_build_model(hp):
    model = keras.Sequential()
    model.add(layers.Flatten())
    model.add(
        layers.Dense(
            # Tune number of units.
            units=hp.Int("units", min_value=32, max_value=512, step=32),
            # Tune the activation function to use.
            activation=hp.Choice("activation", ["relu", "tanh"]),
        )
    )
    # Tune whether to use dropout.
    if hp.Boolean("dropout"):
        model.add(layers.Dropout(rate=0.25))
    model.add(layers.Dense(10, activation="softmax"))
    # Define the optimizer learning rate as a hyperparameter.
    # 2*10e-5
    learning_rate = hp.Float("lr", min_value=1e-4, max_value=1e-2, sampling="log")
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
        loss="categorical_crossentropy",
        metrics=["accuracy"],
    )
    return model


def call_existing_code(units, activation, dropout, lr):
    model = keras.Sequential()
    model.add(layers.Flatten())
    model.add(layers.Dense(units=units, activation=activation))
    if dropout:
        model.add(layers.Dropout(rate=0.25))
    model.add(layers.Dense(10, activation="softmax"))
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=lr),
        loss="categorical_crossentropy",
        metrics=["accuracy"],
    )
    return model


def build_on_existing_model(hp):
    units = hp.Int("units", min_value=32, max_value=512, step=32)
    activation = hp.Choice("activation", ["relu", "tanh"])
    dropout = hp.Boolean("dropout")
    lr = hp.Float("lr", min_value=1e-4, max_value=1e-2, sampling="log")
    # call existing model-building code with the hyperparameter values.
    model = call_existing_code(
        units=units, activation=activation, dropout=dropout, lr=lr
    )
    return model


# To tune the number of units in different Dense layers separately as different 
# hyperparameters, we give them different names as f"units_{i}".

def build_conditional_model(hp):
    model = keras.Sequential()
    model.add(layers.Flatten())
    # Tune the number of layers.
    for i in range(hp.Int("num_layers", 1, 3)):
        model.add(
            layers.Dense(
                # Tune number of units separately.
                units=hp.Int(f"units_{i}", min_value=32, max_value=512, step=32),
                activation=hp.Choice("activation", ["relu", "tanh"]),
            )
        )
    if hp.Boolean("dropout"):
        model.add(layers.Dropout(rate=0.25))
    model.add(layers.Dense(10, activation="softmax"))
    learning_rate = hp.Float("lr", min_value=1e-4, max_value=1e-2, sampling="log")
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
        loss="categorical_crossentropy",
        metrics=["accuracy"],
    )
    return model


def DSSIM_loss(y_true, y_pred):
    # return 1/2 - tf.reduce_mean(tf.image.ssim(y_true, y_pred, 1.0))/2
    return (1.0 - tf.reduce_mean(tf.image.ssim(y_true, y_pred, 1.0)))/2.0


def mvtec_model(hp, input_shape=(128, 128, 1)):

    # Hyper parameters
    latent_dim = hp.Int("latent_dim", min_value=96, max_value=512, step=32)

    # Fixed parameters
    parameters = dict()
    n_layers = 9
    parameters["filters"] = [32, 32, 32, 64, 64, 128, 64, 32, latent_dim]
    parameters["kernel_size"] = [4, 4, 3, 4, 3, 4, 3, 3, 8]
    parameters["strides"] = [2, 2, 1, 2, 1, 2, 1, 1, 1]
    parameters["padding"] = ["same" for _ in range(n_layers-1)] + ["valid"]

    # Input
    inputs = Input(shape=input_shape)
    x = inputs

    # Encoder
    for i in range(0, n_layers):
        x = Conv2D(
          filters=parameters["filters"][i],
          kernel_size=parameters["kernel_size"][i],
          strides=parameters["strides"][i],
          padding=parameters["padding"][i])(x)
        x = LeakyReLU(alpha=0.2)(x)

    # Decoder
    for i in reversed(range(0, n_layers)):
        x = Conv2DTranspose(
          filters=parameters["filters"][i],
          kernel_size=parameters["kernel_size"][i],
          strides=parameters["strides"][i],
          padding=parameters["padding"][i])(x)
        x = LeakyReLU(alpha=0.2)(x)

    # Output
    x = Conv2DTranspose(
      filters=input_shape[2],
      kernel_size=(3, 3),
      strides=(1, 1),
      padding="same")(x)

    outputs = x

    # Autoencoder
    autoencoder = Model(inputs, outputs, name="autoencoder")

    return autoencoder

def mvtec_model_2(hp, input_shape=(128, 128, 1)):

    # Hyper parameters
    # latent_dim = hp.Int("latent_dim", min_value=96, max_value=512, step=32)
    latent_dim = hp.Int("latent_dim", min_value=32, max_value=128, step=32)

    # Fixed parameters
    parameters = dict()
    n_layers = 9
    parameters["filters"] = [32, 32, 32, 64, 64, 128, 64, 32, latent_dim]
    parameters["kernel_size"] = [4, 4, 3, 4, 3, 4, 3, 3, 8]
    parameters["strides"] = [2, 2, 1, 2, 1, 2, 1, 1, 1]
    parameters["padding"] = ["same" for _ in range(n_layers-1)] + ["valid"]

    model = keras.Sequential(name="autoencoder2")

    # Input
    inputs = Input(shape=input_shape)
    # x = inputs
    model.add(inputs)

    # Encoder
    for i in range(0, n_layers):
        model.add( 
            Conv2D(
              filters=parameters["filters"][i],
              kernel_size=parameters["kernel_size"][i],
              strides=parameters["strides"][i],
              padding=parameters["padding"][i]
              )
            )
        model.add( LeakyReLU(alpha=0.2) )
        # x = Conv2D(
        #   filters=parameters["filters"][i],
        #   kernel_size=parameters["kernel_size"][i],
        #   strides=parameters["strides"][i],
        #   padding=parameters["padding"][i])(x)
        # x = LeakyReLU(alpha=0.2)(x)

    # Decoder
    for i in reversed(range(0, n_layers)):
        model.add( 
            Conv2DTranspose(
              filters=parameters["filters"][i],
              kernel_size=parameters["kernel_size"][i],
              strides=parameters["strides"][i],
              padding=parameters["padding"][i]
              )
            )
        model.add( LeakyReLU(alpha=0.2) )
        # x = Conv2DTranspose(
        #   filters=parameters["filters"][i],
        #   kernel_size=parameters["kernel_size"][i],
        #   strides=parameters["strides"][i],
        #   padding=parameters["padding"][i])(x)
        # x = LeakyReLU(alpha=0.2)(x)

    # Output
    model.add( 
        Conv2DTranspose(
          filters=input_shape[2],
          kernel_size=(3, 3),
          strides=(1, 1),
          padding="same"
          )
        )
    # x = Conv2DTranspose(
    #   filters=input_shape[2],
    #   kernel_size=(3, 3),
    #   strides=(1, 1),
    #   padding="same")(x)

    # outputs = x

    # Autoencoder
    # autoencoder = Model(inputs, outputs, name="autoencoder")
    autoencoder = model

    return autoencoder


def build_model(hp: kt.HyperParameters):
    """Here we build and compile the model. We use the `hp` hyperparameter
    class to set up the model that includes an optimizer and loss function.
    Both optimizer and cost functions also have hyperparameter. One issue
    was setting up the cost function. The `Choice` can only contain `int`, 
    `float`, `str`, or `bool`. So we cannot use it. One way to circumvent 
    this is shown [here](https://github.com/keras-team/keras-tuner/issues/358).
    Basically we add a conditional whose condition is derived from a `Choice`
    consisting only of strings."""

    input_shape=dp.Properties.input_shape
    # autoencoder = mvtec_model(hp, input_shape=input_shape)
    autoencoder = mvtec_model_2(hp, input_shape=input_shape)
    autoencoder.summary()

    learning_rate = hp.Float("learning_rate", min_value=2*10e-5, max_value=1e-2, sampling="log")
    opt = Adam(learning_rate=learning_rate)
    # https://stackoverflow.com/questions/67752144/custom-loss-with-external-parameters-in-keras-tuner
    # https://github.com/keras-team/keras-tuner/issues/358
    # autoencoder.compile(loss=training_loss, optimizer=opt)
    # training_loss = hp.Choice("training_loss", ["mse", "dssim"])
    # if training_loss == "mse":
    #       loss_fn = "mse"
    # else:
    #     loss_fn=DSSIM_loss
    # autoencoder.compile(loss=loss_fn, optimizer=opt)
    autoencoder.compile(loss=DSSIM_loss, optimizer=opt)
    # autoencoder.compile(loss="mse", optimizer=opt)

    return autoencoder



def train_data(dataset_name="carpet", random_crop=False, n_train=10000):

  # Load data
  print('Loading data')
  if random_crop:
      x_train = train.read_data_with_random_crop(dataset_name=dataset_name, n_train=n_train)
      x_train = np.expand_dims(x_train, axis=-1)
  else:
      train_data, _ = train.read_data(dataset_name=dataset_name)
      x_train = []
      for image_name in train_data['good'].keys():
          for image in train_data['good'][image_name]:
              x_train.append(image)
      x_train = np.array(x_train)
      x_train = np.expand_dims(x_train, axis=-1)

  # Split into train and evaluation
  print('Splitting data into train and validation sets')
  y_train = x_train
  x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.2, shuffle=True)
  return x_train, x_validation, y_train, y_validation


def parse_args():
    parser = argparse.ArgumentParser('AE_SSIM')
    parser.add_argument("--dataset_name", type=str, default="carpet")
    parser.add_argument("--latent_dim", type=int, default=100)
    parser.add_argument("--batch_size", type=int, default=8)
    parser.add_argument("--training_loss", type=str, default="ssim")
    parser.add_argument("--load_model", type=bool, default=False, action=argparse.BooleanOptionalAction)
    parser.add_argument("--random_crop", type=bool, default=True, action=argparse.BooleanOptionalAction)
    parser.add_argument("--use_tf_dataset", type=bool, default=False, action=argparse.BooleanOptionalAction)
    parser.add_argument("--n_train", type=int, default=10000)
    parser.add_argument("--max_epochs", type=int, default=200)
    parser.add_argument("--n_workers", type=int, default=16)
    return parser.parse_args()


if __name__ == "__main__":
  print("AE Tune")
  args = parse_args()
  print(args)

  print(
      f"\ndataset_name={args.dataset_name}\ntraining_loss={args.training_loss}\n"
      f"latent_dim={args.latent_dim}\nbatch_size={args.batch_size}\n"
      f"load_model={args.load_model}\nrandom_crop={args.random_crop}\n"
      f"use_tf_dataset={args.use_tf_dataset}\nn_train={args.n_train}\n"
      f"max_epochs={args.max_epochs}\n"
      )

  # example_model = example_build_model(kt.HyperParameters())
  # hp = kt.HyperParameters()
  # print(hp.Int("units", min_value=32, max_value=512, step=32))
  # existing_model = build_on_existing_model(kt.HyperParameters())
  # conditional_model = build_conditional_model(kt.HyperParameters())

  tune_dir = f"{dp.Properties.results_dir}/{args.dataset_name}/tune/"
  try:
      os.makedirs(tune_dir)
  except:
      pass

#   timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
  timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M')

  # NOTE: weird behavior if we leave the `tuner` variables assigned (not commented)
  # It seems like all defined tuners are activated and executed. Most probably due
  # to internal state of Keras

  # TODO: add max_trials and executions_per_trial to as a command line argument
  # https://keras.io/guides/keras_tuner/getting_started/
  # https://keras.io/api/keras_tuner/tuners/random/
  print('Setting up search strategy')
  # tuner = kt.RandomSearch(
  #               # mvtec_model, # Ok
  #               build_model,
  #               objective = kt.Objective('val_loss', direction="min"),
  #               max_trials = 10,
  #               executions_per_trial = 2,
  #               # https://keras.io/guides/keras_tuner/distributed_tuning/
  #               # make false
  #               # overwrite = True,
  #               overwrite = False,
  #               directory=tune_dir,
  #               project_name="AE_tune_random" + timestamp
  #             )

  tuner = kt.BayesianOptimization(
                  hypermodel=build_model, # None
                  objective=kt.Objective('val_loss', direction="min"), # None
                  max_trials=50, #10 #220
                  num_initial_points=None, #  value of 3 times the dimensionality of the hyperparameter space is used.
                  alpha=0.0001, # 1e-4.
                  beta=2.6, # (2.6) balancing factor of exploration and exploitation. The larger it is, the more explorative it is
                  seed=None, 
                  hyperparameters=None, # (None) Can be used to override (or register in advance) hyperparameters in the search space.
                  tune_new_entries=True, # Boolean (True), whether hyperparameter entries that are requested by the hypermodel but that were not specified in hyperparameters
                  allow_new_entries=True, # Boolean (True), whether the hypermodel is allowed to request hyperparameter entries not listed in hyperparameters.
                  max_retries_per_trial=0, #  Defaults to 0. The maximum number of times to retry a Trial if the trial crashed or the results are invalid.
                  max_consecutive_failed_trials=3, # Defaults to 3. The maximum number of consecutive failed Trials. When this number is reached, the search will be stopped.
                  # **kwargs
                  # https://keras.io/api/keras_tuner/tuners/base_tuner/#tuner-class
                  executions_per_trial=1,
                  overwrite = False,
                  directory=tune_dir,
                  project_name="AE_tune_bayes" + timestamp
                )

#   tuner = kt.Hyperband(
#                   hypermodel=build_model, # None
#                   objective=kt.Objective('val_loss', direction="min"), # None
#                   # recommended to set this to a value slightly higher than 
#                   # the expected epochs to convergence for your largest Model, 
#                   # and to use early stopping during training 
#                   max_epochs=200, # =100,
#                   factor=3, # =3, reduction factor for the number of epochs and number of models for each bracket
#                   # One iteration will run approximately max_epochs * (math.log(max_epochs, factor) ** 2) cumulative epochs across all trials
#                   # 200×(log(200,3)^2) = 1059,547038468
#                   # set as high as possible
#                   hyperband_iterations=1, # =1,  
#                   seed=None, # =None,
#                   hyperparameters=None, # =None, 
#                   tune_new_entries=True, # whether hyperparameter entries that are requested by the hypermodel but that were not specified in hyperparameters should be added to the search 
#                   allow_new_entries=True, # , whether the hypermodel is allowed to request hyperparameter entries not listed in hyperparameters
#                   max_retries_per_trial=0,
#                   max_consecutive_failed_trials=3,
#                   # https://keras.io/api/keras_tuner/tuners/base_tuner/#tuner-class
#                   executions_per_trial=1,
#                   overwrite = False,
#                   directory=tune_dir,
#                   project_name="AE_tune_hyperband" + timestamp
#             )
# 
  print('Printing search space summary')
  tuner.search_space_summary()

  if not args.use_tf_dataset:
    x_train, x_validation, y_train, y_validation = train_data(
                                                      dataset_name=args.dataset_name, 
                                                      random_crop=args.random_crop,
                                                      n_train=args.n_train
                                                    )
  else:
    props = dp.MTVecProps()
    pipe1 = tfp.TFPipe(props)
    data = pipe1.load_data(datasets_names=[args.dataset_name], load_image_fn = pipe1.load_image_train)
    print(f"Loaded data: data.image_count={data.image_count}, data.train_count={data.train_count}, data.validation_count={data.validation_count}")
  
  path_to_save_model_dir = f"model_weights/{args.dataset_name}/" + timestamp + "/"

  # Keras Tuner will handle all the checkpoints

  # https://www.tensorflow.org/tutorials/keras/keras_tuner
  early_stopping_callback = EarlyStopping(
                                  monitor='val_loss', 
                                  min_delta=0, 
                                  patience=3, # original 50
                                  verbose=0, 
                                  mode='auto',
                                  restore_best_weights=True, # default False
                                  #start_from_epoch=0
                              )

  # Keras tuner automatically add the HDParams for visualization
  # https://keras.io/guides/keras_tuner/visualize_tuning/
  # https://www.tensorflow.org/tensorboard/hyperparameter_tuning_with_hparams
  tensorboard_callback = keras.callbacks.TensorBoard(
                                  log_dir=path_to_save_model_dir,
                                  #write_graph=False, # avoid large log file
                                  write_graph=True,
                                  write_images=True,
                                  histogram_freq=1,  # How often to log histogram visualizations
                                  embeddings_freq=2,  # How often to log embedding visualizations
                                  # profile_batch=(1,200), # Profile the batch(es) to sample compute characteristics. Default off   
                                  update_freq="epoch", # batch, epoch or number for unit count of metrics
                              )  # How often to write logs (default: once per epoch)

  if not args.use_tf_dataset:
    # https://keras.io/api/keras_tuner/tuners/
    # https://keras.io/api/keras_tuner/tuners/base_tuner/#tuner-class
    tuner.search(
                x_train,
                y_train,
                epochs=args.max_epochs,
                validation_data=(x_validation, y_validation),
                verbose=0,
                use_multiprocessing=True,
                workers=args.n_workers,
                shuffle=True,
                batch_size=args.batch_size,
                # Use the TensorBoard callback.
                callbacks=[ early_stopping_callback, tensorboard_callback ],
                )
  else:
    print("Using TF data.Dataset")
    print(f"args.batch_size={args.batch_size}")
    print(f"steps_per_epoch={props.steps_per_epoch}"),
    print(f"validation_steps={props.validation_steps}"),
    print(f"workers={args.n_workers}"),
    tuner.search(
                x=data.train_ds.batch(args.batch_size), 
                epochs=args.max_epochs,
                steps_per_epoch=props.steps_per_epoch,
                validation_data=data.val_ds.batch(args.batch_size), #.take(validation_size),
                validation_steps=props.validation_steps,
                verbose=0,
                use_multiprocessing=True,
                workers=args.n_workers,
                shuffle=True,
                batch_size=args.batch_size,
                # Use the TensorBoard callback.
                callbacks=[ early_stopping_callback, tensorboard_callback],
                )

  print('\nQuerying search results')
  tuner.results_summary()

  sys.stdout.flush()
  sys.stderr.flush()

  print('\nGets best models')
  # Get the top 2 models.
  models = tuner.get_best_models(num_models=1)
  best_model = models[0]
  # Build the model.
  # Needed for `Sequential` without specified `input_shape`.
  best_model.build(input_shape=dp.Properties.input_shape)
  best_model.summary()

  # Retrain the model
  print('\nRetrain the model')
  # Get the top 2 hyperparameters.
  best_hps = tuner.get_best_hyperparameters(5)
  best = best_hps[0]
  print(f"Best latent_dim: {best.get('latent_dim')}")
  print(f"Best learning_rate: {best.get('learning_rate')}")
  # Build the model with the best hp.
  model = build_model(best_hps[0])
  # Fit with the entire dataset.
  x_all = np.concatenate((x_train, x_validation))
  y_all = np.concatenate((y_train, y_validation))
  # TODO: use common training setup
  # model.fit(x=x_all, y=y_all, epochs=max_epochs)

  # https://keras.io/guides/keras_tuner/distributed_tuning/
  # https://www.tensorflow.org/tutorials/distribute/multi_worker_with_keras

# https://keras.io/api/preprocessing/image/


