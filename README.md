<!--- cSpell: ignore SSIM, TP, TN, FP, FN, FPR, TPR, Younden, autoencoders -->

# Unsupervised-Anomaly-Detection-with-SSIM-AE

## Story on Medium
[Anomaly Detection in Computer Vision with SSIM-AE](https://medium.com/@majpaw1996/anomaly-detection-in-computer-vision-with-ssim-ae-2d5256ffc06b)

## Original code

[Unsupervised-Anomaly-Detection-with-SSIM-AE](https://github.com/PabloMaj/Unsupervised-Anomaly-Detection-with-SSIM-AE)

Some changes in the evaluation:

1. Added the `good` class to the evaluation
1. The TP, TN, FP and FN are summed over all images
1. The Younden and DICE metrics are not calculate per image (FPR and TPR are still calculated per image). The Youden metrics would be indeterminate for images of the class `good` that correctly did not have FP.
1. TPR and FPR check for a 0 denominator and set the value assuming a correct label.
1. The Younden and DICE metrics now use the sum of the TP, TN, FP and FN counts (they are **not** the average per image)
1. The mean TPR and FPR are still used for the ROC plot
1. The ROC plot now shows the maxArg index position and values
1. The metrics summary of each evaluation is written to a `results/metrics.txt` file
1. For each image, a CSV file is recorded with the experiments threshold, TP, TN, FP, FN, TPR ad FPR counts. It also includes links to the original image, the predicted image and the heatmap of the reconstruction error. 
1. Minor coding clean up to pass classes instead of separate lists

Some changes in the training:

1. Added `TensorBoard` callback
1. The `patience` parameter of `EarlyStopping` was changed from `50` to `3`
1. Corrected bug when parsing the `--load_model` and `--random_crop`command line arguments
1. Added scripts to start training in the background
1. All training output (stdin and stdout) is written to files (respectively `model_weights/error.txt` and `model_weights/output.txt`)
1. The `model_weights/output.txt` contains the list of `loss` and `val_loss` values and the selected epoch (history from the `fit` function)
1. Minor refactoring of paths
1. Minor changes to import to remove linting warnings


## Related Works

1. Bergmann, Paul, et al. "Improving unsupervised defect segmentation by applying structural similarity to autoencoders." [arXiv preprint arXiv:1807.02011](https://arxiv.org/abs/1807.02011). Lates version from 2019.

2. Bergmann, Paul, et al. "MVTec AD--A Comprehensive Real-World Dataset for Unsupervised Anomaly Detection." [Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2019](https://openaccess.thecvf.com/content_CVPR_2019/papers/Bergmann_MVTec_AD_--_A_Comprehensive_Real-World_Dataset_for_Unsupervised_Anomaly_CVPR_2019_paper.pdf)

3. Paul Bergmann, Sindy Löwe, Michael Fauser, David Sattlegger, Carsten Steger: Improving Unsupervised Defect Segmentation by Applying Structural Similarity to Autoencoders; in: Proceedings of the 14th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications (VISIGRAPP), Volume 5: VISAPP, 372-380, 2019, [DOI: 10.5220/0007364503720380](https://www.scitepress.org/Link.aspx?doi=10.5220/0007364503720380). [Click here for supplementary material](https://www.mydrive.ch/shares/46066/8338a11f32bb1b7b215c5381abe54ebf/download/420939225-1629955758/textures.zip).


## Data

Used datasets are available on the websites:
- *grid* and *carpet*: https://www.mvtec.com/company/research/datasets/mvtec-ad/,
- *woven fabrics* (*texture_1* and *texture_2*): https://www.mvtec.com/company/research/publications/

Put downloaded datasets in directory *data/*

## Training

python AE_training.py 
[-h] [--dataset_name DATASET_NAME] [--latent_dim LATENT_DIM]
[--batch_size BATCH_SIZE] [--training_loss TRAINING_LOSS]
[--load_model LOAD_MODEL] [--random_crop RANDOM_CROP]

**Parameters**:
- dataset_name (name of dataset used for training) e.g. "grid", "carpet", "texture_1", "texture_2",
- latent_dim (dimension of bottleneck in autoencoder architecture) e.g. 100,
- batch_size (batch size used for autoencoder training) e.g. 8,
- training_loss (loss used for autoencoder training): "ssim" or "mse",
- load_model (load weights of trained model): 1 or 0,
- random_crop (random crop 10k ROIs of size 128): 1 or 0.

## Evaluation

python AE_evaluation.py
