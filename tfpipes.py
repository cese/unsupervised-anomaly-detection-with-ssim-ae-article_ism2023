
# cSpell: ignore AUC, younden, mtvec, dtype, TF, tensorboard

import textwrap
import inspect
import pathlib
import sys
import os
import os.path
import glob
import itertools as it
from datetime import datetime
import shutil
import math
import random
from typing import Tuple

import tensorflow as tf
import numpy as np


import AE_training as train
import properties as p


class TFPipeResult:
    def __init__(self, train_ds: tf.data.Dataset, val_ds: tf.data.Dataset, image_count: int, validation_count: int, train_count: int):
        self.train_ds = train_ds
        self.val_ds = val_ds
        self.image_count = image_count
        self.validation_count = validation_count
        self.train_count = train_count


class TFPipe:
    """Implements a simple image augmentation pipeline that loads the images 
    and creates """

    GOOD = "good"
    BAD = "bad"

    def __init__(self, properties: p.Properties):
        self.props = properties

        self.train_dest = self.props.map_samples["train"]
        self.test_dest = self.props.map_samples["test"]
        self.ground_truth_dest = self.props.map_samples["ground_truth"]

        self.dest_train_good = os.path.join(self.train_dest,         self.GOOD)
        self.dest_train_bad = os.path.join(self.train_dest,         self.BAD)
        self.dest_test_good = os.path.join(self.test_dest,          self.GOOD)
        self.dest_test_bad = os.path.join(self.test_dest,          self.BAD)
        self.dest_ground_good = os.path.join(self.ground_truth_dest,  self.GOOD)
        self.dest_ground_bad = os.path.join(self.ground_truth_dest,  self.BAD)

        self.cnt = 0

    # https://www.tensorflow.org/api_docs/python/tf/image
    supported = [
        "*.bmp",
        "*.gif",
        "*.jpeg",
        "*.jpg",
        "*.png",
    ]

    # https://stackoverflow.com/questions/4568580/python-glob-multiple-filetypes
    def multiple_file_types(*patterns):
        p = [glob.iglob(pattern) for pattern in patterns]
        return it.chain.from_iterable(p)

    # https://stackoverflow.com/questions/4568580/python-glob-multiple-filetypes
    def images_only(self, root: str, patterns, recursive=True):
        """Get iterator of all image files found recursively under the 'root' folder"""
        ps = [os.path.join(root, pattern) for pattern in patterns]
        # use iglob and not glob
        return it.chain.from_iterable(glob.iglob(p, recursive=recursive) for p in ps)

    def create_dest(self, path: str):
        # if not os.path.exists(path):
        #     os.mkdir(path)
        #     print(f"Created folder: {path}")
        os.makedirs(path, exist_ok=True)
        print(f"Created folder: {path}")

    def make_file_name(self, dest: str, dataset: str, original_file: str) -> tuple[str, str]:
        """Changes the destination folder by splicing the category (class) and dataset name 
        into a new path. This ensures that all files are placed into a top tier `train`,
        `test` and `truth` folder. Below that we have category `good` (no defects) or 
        `bad` (with defects). Here is an example:
          * /data/test/good/
          * /data/test/bad/
          * /data/train/good/
          * /data/train/bad/
          * /data/truth/good/
          * /data/truth/bad/
        Note that dest is one of /data/{test, train or truth}/{good or bad}
        The filenames are also changed. It splices the dataset name and category to 
        the filename. If the file name is a mask file, it ensures that the `_mask` 
        also appears in the name. It assumes the original paths (original_file) are
        as follows:
          * /path_to_data/dataset/{category}/{name}.{ext}
          * /path_to_data/{category}/{name}.{ext}
          * /path_to_data/dataset/{category}/{name}_mask.{ext}
          * /path_to_data/{category}/{name}_mask.{ext}
        are converted to:
          * /dest/dataset/{test, train}/good/{dataset}_{name}.{ext}
          * /dest/dataset/{test, train}/bad/{dataset}_{category}_{name}.{ext}
          * /dest/dataset/truth/{dataset}_{category}_{name}_mask.{ext}
        Note that `truth` only contain anomalies so no `good` data exists.
        """
        splits = original_file.split(os.sep)
        category = splits[len(splits)-2]
        file_name = splits[len(splits)-1]

        # if original_file.find('texture') >= 0:
        if dataset.find('texture') >= 0 and original_file.find('truth') >= 0:
            file_basename = file_name.split('.')
            # remove the extension
            file_extension = file_basename[len(file_basename)-1]
            file_basename = file_basename[len(file_basename)-2]
            # add the '_mask' section
            file_name = file_basename + '_mask.' + file_extension

        if category == self.GOOD:
            folder = self.GOOD
            category = ""
        else:
            folder = self.BAD
            category = category + "_"

        dest = os.path.join(dest, folder, dataset + '_' + category + file_name)
        return (dest, category)

    def rename(self, dest_ground_bad, original_file: str) -> str:
        """Changes the destination filename by splicing `_mask` because
        all copied files of `truth` were renamed to include `_mask` at
        the end.
        """
        # get the file name
        splits = original_file.split(os.sep)
        file_name = splits[len(splits)-1]
        file_basename = file_name.split('.')
        # remove the extension
        file_extension = file_basename[len(file_basename)-1]
        file_basename = file_basename[len(file_basename)-2]
        # add the '_mask' section
        file_basename = file_basename + '_mask.' + file_extension
        # return the full name
        # path = splits[0:len(splits)-2]
        # print(f'path = {path}')
        # mask_file = os.path.join(*path, file_basename)
        mask_file = os.path.join(dest_ground_bad, file_basename)
        return mask_file

    def check_copies(self, count):
        """Here we count the number of files that are found in the destination
        folder. This count must match the number of original files that were 
        copied. We use this check to make sure files with different content but 
        the same name don't get overridden."""
        print('Checking files copied correctly', end=" ")
        destination = os.path.join(self.props.root, '*/**')
        copied_files = self.images_only(destination, self.supported)
        count_copied = len(list(copied_files))
        if count != count_copied:
            raise RuntimeError(f'Copied {count} file but found {count_copied}')
        else:
            print("...Ok")

    def check_corresponding_masks(self):
        """Here we check that all mask files (truth) have a corresponding
        `bad` sample. We get the list of copied mask image files, remove the 
        `_mask` from those names and the check if these new exist in the 
        destination folders. Note that all `truth` filenames terminate with
         `_mask`, so it is guaranteed to exist."""
        print('Checking masked files match "bad" files', end=" ")
        # get source files
        copied_files = self.images_only(self.dest_test_bad, self.supported)
        copied_files = map(lambda d: self.rename(
            self.dest_ground_bad, d), copied_files)
        # get destination files (were copied)
        mask_files = self.images_only(self.dest_ground_bad, self.supported)
        copied_files = set(copied_files)
        mask_files = set(mask_files)
        # In source but not destination
        diff_1 = copied_files - mask_files
        # In destination but not destination
        diff_2 = mask_files - copied_files
        len_diff_1 = len(diff_1)
        len_diff_2 = len(diff_2)
        # are_equal = copied_files == mask_files
        # if not are_equal:
        if len_diff_1 > 0 or len_diff_2 > 0:
            if len_diff_1 > 0:
                print(
                    f"Copied but not in destination: {len_diff_1} (some examples)")
                for x in list(diff_1)[0: 10]:
                    print(x)

            if len_diff_2 > 0:
                print(
                    f"In destination but not in source: {len_diff_2} (some examples)")
                for x in list(diff_2)[0:10]:
                    print(x)
            raise RuntimeError('Test files and mask files do not match.')
        else:
            print("...Ok")

    def create_destination_folders(self):
        """Create the destination folders if necessary. We have 
        the training, test and validation data. The validation 
        data consists of image masks that match anomalies in
        the test data."""
        self.create_dest(self.dest_train_good)
        self.create_dest(self.dest_train_bad)
        self.create_dest(self.dest_test_good)
        self.create_dest(self.dest_test_bad)
        self.create_dest(self.dest_ground_good)
        self.create_dest(self.dest_ground_bad)

    def copy_files(self):
        """Here we copy the files from the original cache sources to this project's
        local cache directory. The files are renamed if necessary to avoid 
        overwriting previously copied files. The data is split into 3 folders:
        - /data/test
        - /data/train
        - /data/truth
        In each folder we have the `good` and `bad` folders were the images 
        containing no defects and with defects are stored. The test set 
        has images with and without defects. The train set has images with no 
        defects. The `truth` folder has the same defect images found in test
        set. All files names in the end with `test/bad` exist in the `truth`
        folders but end with `_mask`. 
        """

        print('Copying fils to destination')

        # Create the destination folders if they don't exist
        self.create_destination_folders()

        # Get the list of files from the dataset cache
        source = os.path.join(self.props.data_cache, '*/**')
        files = self.images_only(source, self.supported)

        # Go through list of files and copy to the correct destination
        # Rename the files if necessary to avoid overwriting
        count = 0
        bads = {}
        for f in files:

            # Copy training data
            (b, n) = self.props.is_training_sample(f)
            if b:
                # shutil.copy(f, train_dest)
                (dest, _) = self.make_file_name(self.train_dest, n, f)
                shutil.copyfile(f, dest)
                print(f"{self.props.dataset}, {n}, {f}, {dest}")
                count += 1
            else:
                # Copy test data
                (b, n) = self.props.is_test_sample(f)
                if b:
                    # shutil.copy(f, test_dest)
                    (dest, c) = self.make_file_name(self.test_dest, n, f)
                    shutil.copyfile(f, dest)
                    print(f"{props.dataset}, {n}, {f}, {dest}")
                    count += 1
                    if c == self.BAD:
                        bads.add(c)
                else:
                    # Copy ground truth of test data
                    (b, n) = self.props.is_ground_truth(f)
                    if b:
                        # shutil.copy(f, ground_truth_dest)
                        (dest, _) = self.make_file_name(
                            self.ground_truth_dest, n, f)
                        shutil.copyfile(f, dest)
                        print(f"{self.props.dataset}, {n}, {f}, {dest}")
                        count += 1
                    # else:
                    #   print(f", {f}, , ")
                    #   count += 1

        print('..finished.')
        return count

    # https://machinelearningmastery.com/how-to-manually-scale-image-pixel-data-for-deep-learning/
    # https://www.geeksforgeeks.org/how-to-normalize-center-and-standardize-image-pixels-in-keras/
    # Pixel Normalization – Scales values of the pixels in 0-1 range.
    # Pixel Centering – Scales values of the pixels to have a 0 mean.
    # Pixel Standardization – Scales values of the pixels to have 0 mean and unit (1) variance.

    # https://dlhr.de/9
    # Same as normalize when min and max are different. Range [0..1]
    def rescale_0_1(self, tensor: tf.Tensor) -> tf.Tensor:
        # tensor = tf.cast(tensor, tf.float64)
        tensor = (tensor - tf.math.reduce_min(tensor)) * \
            (1 / (tf.math.reduce_max(tensor) - tf.math.reduce_min(tensor)))
        return tensor

    # Mean is 0 but range extends from negative to positive values
    def center(self, tensor: tf.Tensor) -> tf.Tensor:
        # tensor = tf.cast(tensor, tf.float64)
        tensor = tensor - tf.math.reduce_mean(tensor)
        return tensor

    def standardization(self, tensor: tf.Tensor) -> tf.Tensor:
        return tf.image.per_image_standardization(tensor)

    def positive_standardization(self, tensor: tf.Tensor) -> tf.Tensor:
        tensor = tf.image.per_image_standardization(tensor)
        tensor = tf.clip_by_value(tensor, clip_value_min=-1, clip_value_max=1)
        tensor = (tensor + 1.0) / 2.0
        return tensor


    # https://www.activestate.com/resources/quick-reads/how-to-debug-tensorflow/
    # 
    @tf.function  
    def debug(self, has_textures: bool):
        """This function decides what the resize dimensions are for a 
        a specific dataset. It simply reimplements the Python call to
        TFPipes: self.props.resize_dims(path). The has_textures 
        parameter is a boolean that is true if we are using the 
        texture datasets or false otherwise. This is a crude kludge 
        used to set this value. 

        Quoting
        "Eager execution is enabled by default and this API returns 
         True in most of cases. However, this API might return False 
         in the following use cases.

            * Executing inside tf.function, unless under tf.init_scope or 
              tf.config.run_functions_eagerly(True) is previously called.
            * Executing inside a transformation function for tf.dataset.
            * tf.compat.v1.disable_eager_execution() is called.

        "For TF V2.x.x use tf.py_function or tf.numpy_function instead of tf.py_func." 
        "You can also use tf.py_function to debug your models at runtime using Python tools"
        See:
            https://stackoverflow.com/questions/56122670/how-to-get-string-value-out-of-tf-tensor-which-dtype-is-string 
            https://www.tensorflow.org/api_docs/python/tf/py_function
            https://www.tensorflow.org/guide/effective_tf2
            https://github.com/tensorflow/tensorflow/issues/40309

        TODO: find a better way to set parameters per 
        dataset/category

        For more information see:
            https://www.tensorflow.org/guide/function
            https://www.tensorflow.org/guide/function#debugging
            https://www.tensorflow.org/guide/function#limitations
            https://www.tensorflow.org/api_docs/python/tf/executing_eagerly
        """
        # Still C++ Tensors, so still cannot look into them 
        # print(f"a = {a}")
        # dims = self.props.resize_dims(a)
        # return dims
        # print(f"has_textures = {has_textures}")
        # So test the values in python (auto-transformed because it is a parameter)
        if has_textures:
            img_resized_size = (256, 256)
            # tf.print("D: Texture", output_stream=sys.stdout)
        else:
            img_resized_size = (512, 512)
            # tf.print("D: Other", output_stream=sys.stdout)
        return img_resized_size

    def load_image(self, file_path: str) -> Tuple[tf.Tensor, tf.Tensor, tf.Tensor, tf.Tensor]:
        """This function is called via a map in the TF pipeline. It samples the images
        and preprocesses them for training. It performs the following:

        * Loads the data of the `file_path` file
        * Decodes the data as an image
        * Resizes the image to `self.props.resize_dims`
        * Converts the image to grayscale
        * Rescales, normalizes or standardizes the image (crop = crop.astype("float32") / 255.0)

        TODO: add random rotations (https://www.tensorflow.org/guide/data#applying_arbitrary_python_logic)
        TODO: add random flips
        TODO: add random zoom?
        TODO: add other augmentations (filter, warp, HSV conversion)
        see https://www.tensorflow.org/addons/tutorials/image_ops
        see https://towardsai.net/p/machine-learning/building-complex-image-augmentation-pipelines-with-tensorflow-bed1914278d2

        Note that some care must be taken when normalizing the images, if we want to vie them properly. 
        see https://stackoverflow.com/questions/46217420/my-picture-after-using-tf-image-resize-images-becomes-horrible-picture

        NOTE: printing here does not work. Compiled by TF for eager execution. 
        NOTE: https://www.tensorflow.org/guide/data#datasets_sampling
        """

        # Load the raw data from the file as a string
        image = tf.io.read_file(file_path)
        # Convert the compressed string to a 3D uint8 tensor
        # https://www.tensorflow.org/api_docs/python/tf/io/decode_image
        # image_tensor = tf.io.decode(image, channels=3, expand_animations=False)
        image_tensor_orig = tf.io.decode_image(image, expand_animations=False)

        # Cannot pass back a TensorShape
        # https://github.com/tensorflow/tensorflow/issues/8551
        # https://github.com/tensorflow/tensorflow/issues/9356
        # shape is unknown at this point
        # shape = tf.convert_to_tensor(image_tensor.shape, dtype=tf.int64)
        shape = tf.shape(image_tensor_orig)
        channels = shape[2]

        # Resize the image to the desired size
        # https://www.tensorflow.org/api_docs/python/tf/image/resize
        # The return value has type float32, unless the method is
        # ResizeMethod.NEAREST_NEIGHBOR, then the return dtype is the
        # dtype of images
        # https://stackoverflow.com/questions/42144915/convert-tensorflow-string-to-python-string
        # AttributeError: 'Tensor' object has no attribute 'numpy'
        # must be in eager operation
        # path = file_path.numpy().decode('UTF-8') # .decode('ascii') 
        # https://www.tensorflow.org/api_docs/python/tf/strings/as_string
        # not a valid input
        # path = tf.strings.as_string(file_path).decode('UTF-8')
        # tf.print(f"Get resize dim of {path}", output_stream=sys.stdout)
        # dims = self.props.resize_dims(path)
        # tf.print(f"Resize dim of {path} is: {dims}", output_stream=sys.stdout)
        has_textures = tf.strings.regex_full_match(file_path, ".*texture_.*")
        # https://towardsdatascience.com/using-tf-print-in-tensorflow-aa26e1cff11e
        # https://github.com/tensorflow/tensorflow/issues/43993
        # https://stackoverflow.com/questions/34097281/convert-a-tensor-to-numpy-array-in-tensorflow
        # select = tf.strings.as_string(hasTextures)
        # tf.print(f"Resize dim of {file_path} is: {hasTextures}", output_stream=sys.stdout)
        # No way we can peer into node values, so inline Python code, which is slow
        # if hasTextures:
        #     path = "texture_"
        #     img_resized_size = (256, 256)
        #     # tf.print("Texture", output_stream=sys.stdout)
        # else:
        #     path = "grid"
        #     img_resized_size = (512, 512)
        #     # tf.print("Other", output_stream=sys.stdout)
        # dims = self.debug(file_path)
        # A little faster. Make sure the call is in a useful graph path 
        img_resized_size = self.debug(has_textures)
        # Path is not a python string (is a tensor)
        # dims = self.props.resize_dims(path)
        image_tensor = tf.image.resize(
            image_tensor_orig,
            img_resized_size, #dims, # self.props.resize_dims(file_path),
            method=tf.image.ResizeMethod.BILINEAR,
            preserve_aspect_ratio=False,
            antialias=False,
            name=None)
        # Ensure the image is correct and visible
        # Image viewing software assumes that if data is of type float will have a
        # range between 0 and 1.0 and if it is an integer, it will have a range
        # between 0 and 255. However, resize will by default will return floats between
        # 0 and 255 that are not integer values. By up-casting, the values are truncated
        # to integers, thereby allowing proper image viewing.
        # image_tensor = tf.cast(image_tensor, np.int64)
        image_tensor = tf.cast(image_tensor, np.uint8)

        # Prepare image for sampling and training
        image_tensor = tf.image.convert_image_dtype(image_tensor, tf.float32)

        # Convert to gray scale
        # https://www.tensorflow.org/api_docs/python/tf/image/rgb_to_grayscale
        converted_tensor = tf.cond(
            channels > 1,
            lambda: tf.image.rgb_to_grayscale(image_tensor),
            lambda: image_tensor
        )

        # Data augmentation: we do this after conversion to grayscale so that we
        # need only process one channel instead of 3
        converted_tensor = tf.image.random_crop(
            converted_tensor,
            size=[
                self.props.input_shape[0],
                self.props.input_shape[1],
                self.props.input_shape[2]]
        )

        new_shape = tf.shape(converted_tensor)

        # normalize image
        # https://www.geeksforgeeks.org/how-to-normalize-center-and-standardize-image-pixels-in-keras/
        # https://www.tensorflow.org/api_docs/python/tf/image/per_image_standardization
        # Black images? [0.0007507359..0.002256237]
        # normalized_tensor = converted_tensor / 255.0
        # This won't work because the grayscale images already have values [0..1].
        # However, the values are not spread out (the minimum is not close to 0 and
        # the maximum is not close to 1)
        # Example : [0.17894904 .. 0.49408317] grayscale
        # normalized_tensor = converted_tensor
        # Example : [0.00061514805..0.002522107] grayscale
        # normalized_tensor = converted_tensor / 255.0

        # Sharp images. Looks like a Canny edge detector was used. Mean u = 0
        # range [u - sd .. u + sd], may contain negative values
        # normalized_tensor = tf.image.per_image_standardization(converted_tensor)
        # normalized_tensor = self.standardization(converted_tensor)
        # Better viewing
        # normalized_tensor = self.positive_standardization(converted_tensor)

        # Normal grayscale images [0..1]. Next to best viewing
        normalized_tensor = self.rescale_0_1(converted_tensor)

        # Dark images, the negative values seem to be clipped so only lower values are seen
        # [-0.2280345..0.30764478]
        # normalized_tensor = self.center(converted_tensor)

        # return the result
        return (file_path, shape, normalized_tensor, new_shape)


    def load_image_train(self, file_path: str) -> tf.Tensor:
        """Extract the image from the augmented image result
        and create a tuple of (input,m output) image, using 
        the same image. We want to measure the reconstruction
        error of this image."""
        data = self.load_image(file_path)
        return (data[2], data[2])

    # https://www.tensorflow.org/tutorials/load_data/images

    def configure_for_performance(self, image_count: int, ds: tf.data.Dataset):
        """Add caching, shuffling and prefetching to optimize the IO.
        Apply this only to the loaded images. Do not use this after the 
        data augmentation, because the images are randomly augmented and
        should therefore not be cached."""
        ds = ds.cache()
        # perfect shuffling requires this value to be
        # equal to or greater than the dataset size
        # reshuffle_each_iteration defaults to true
        ds = ds.shuffle(buffer_size=image_count)
        # https://www.tensorflow.org/guide/data_performance#top_of_page
        # If you set the batch (even if 1), the map will get a Tensor
        # We need to process each element of the batch separately
        # ds = ds.batch(self.props.batch_size)
        # ds = ds.batch(1)
        ds = ds.prefetch(buffer_size=tf.data.AUTOTUNE)
        return ds


    def numpy_image(self, tuple_in):
        """Convert the image as a Tensor to a numpy array. 
        We can then use this value directly in Python.
        """
        file_path, shape, image_tensor, new_shape = tuple_in
        image_tensor = image_tensor.numpy()
        return image_tensor



    def sample_and_save_images(
            self,
            log_dir: str,
            ds: tf.data.Dataset,
            number_samples: int
        ):
        """
        The image data is consumed from the TF data.Dataset. `number_samples` 
        samples are removed from this data stream. The number os samples is 
        configurable, however TF only samples to a maximum number of images.
        In fact, each Tensorboard plugin has a set o parameters to control this 
        sampling. To change this value use these commands in the IDE's bash terminal:

            :~$ tensorboard --logdir ./logs --samples_per_plugin "images=0"
            :~$ tensorboard --logdir ./logs --samples_per_plugin "images=1000"
            :~$ tensorboard --logdir ./logs --samples_per_plugin images=1000
            :~$ tensorboard --logdir=./logs --samples_per_plugin "images=100, text=100"
            :~$ tensorboard --logdir=./logs --samples_per_plugin "images=100, text=100" "--load_fast=false"

        see https://www.tensorflow.org/tensorboard/image_summaries

        Note: the `step` parameter of `tf.summary.image` allows us to group 
        images across epochs. One can use this to measure/view the progress.


        see https://github.com/tensorflow/tensorboard/issues/2418
        see https://github.com/tensorflow/tensorflow/issues/7964
        """
        # https://www.tensorflow.org/tensorboard/image_summaries
        # Sets up a timestamped log directory
        log_dir = log_dir + datetime.now().strftime("%Y%m%d-%H%M%S")
        # Creates a file writer for the log directory (if it does not already exist)
        file_writer = tf.summary.create_file_writer(log_dir)

        # Using the file writer, to log the reshaped image.
        # We will use tf.summary.image so we need a tensor with the shape [k, w, h, c]
        #   k - number of images
        #   w,h - width and height
        #   c - number of channels
        with file_writer.as_default():

            print(f"Sampling {number_samples} images")
            data = ds.take(number_samples)

            # convert images to numpy arrays
            print(f"Converting {number_samples} to numpy images")
            images = list(map(self.numpy_image, data))
            print("Shapes and sizes of the images:")
            count=0
            for i in images:
                count += 1
                print(f"\tsample {count} - shape: {i.shape};  size: {i.size}")

            # Get the images shape by using the first image (all are the same)
            image = images[0]
            shape = image.shape
            # reshape all the images to the correct dimension (first dimension 
            # is the number of batches). We need this to save an image(s)
            images = np.reshape(images, (-1, shape[0], shape[1], shape[2]))
            # save all images in the batch. for a single image just use 
            # images[0] and don't set the max_outputs parameter
            tf.summary.image("Training data", images, max_outputs=len(images), step=0)

            print(f"Values range: [{np.min(image)}..{np.max(image)}]")


    # see https://www.tensorflow.org/guide/data
    # def load_train_data(self) -> Tuple[tf.data.Dataset, tf.data.Dataset, int]:
    def load_data(
            self,
            datasets_names: list[str] = list(),
            load_image_fn = load_image
        ) -> TFPipeResult:
        """
        This function loads the image files and generates two infinite TF data.Dataset. The
        number of files are finite, but it performs data augmentation by repeatedly sampling 
        the same images. One set is used for training and the other for validation. Note that 
        the validation dataset is not the testing dataset. 

        For more information on the data augmentation that is performed see:
        - load_image
        - load_image_train

        see https://stackoverflow.com/questions/69281548/when-is-repeat-used-when-loading-a-tensorflow-dataset-for-training
        see https://www.tensorflow.org/guide/data#resampling
        see https://www.tensorflow.org/api_docs/python/tf/data/Dataset
        see https://www.tensorflow.org/api_docs/python/tf/data/Dataset#rejection_resample
        see https://www.tensorflow.org/api_docs/python/tf/data/Dataset#sample_from_datasets
        see https://www.tensorflow.org/tutorials/generative/pix2pix#generate_images

        see https://www.tensorflow.org/tutorials/load_data/images
        see https://www.tensorflow.org/tutorials/load_data/images#configure_the_dataset_for_performance
        see https://www.tensorflow.org/tutorials/images/data_augmentation
        see https://towardsdatascience.com/image-augmentations-in-tensorflow-62967f59239d
        see https://pyimagesearch.com/2021/06/28/data-augmentation-with-tf-data-and-tensorflow/
        """

        print('Loading training data')

        # Get the list of files from the dataset cache
        source = os.path.join(self.train_dest, '*/**')
        files = self.images_only(source, self.supported)

        # filter out the files if required
        if datasets_names:
            print(f"datasets_names = {datasets_names}")
            files = filter( lambda f: self.props.contains_any(f, datasets_names)[0], files )

        # TF uses bytes, so convert string to bytes
        files = map(lambda d: d.encode('UTF-8'), files)

        # The version below does not recurse paths to any depth
        # source = os.path.join(self.props.data_cache, '*/**/*/*')
        # list_ds = tf.data.Dataset.list_files( source, shuffle=False)

        # Count the number of files
        # So we do this manually. Note that we have to use a list
        files = list(files)
        list_ds = tf.data.Dataset.from_tensor_slices(files)
        image_count = len(list_ds)
        # find train -type f | wc -l gave the same result
        print(f"Found {image_count} files.")
        print(f"TestFile {files[0]} of {type(files[0])}")
        test = str(files[0]).find("texture")
        print(f"test(texture) = {test}")

        # Shuffle the image list the first time only
        list_ds = list_ds.shuffle(image_count, reshuffle_each_iteration=False)

        # The original code preprocessed all images and only then split
        # This has 2 issues:
        # - data leakage: we use the same image for training and evaluation (even if we use different crops)
        # - memory usage: we split on images which may duplicate images that occupy more space
        # Split the dataset into training and validation sets:
        validation_size = int(image_count * self.props.validation_split)
        train_ds = list_ds.skip(validation_size)
        val_ds = list_ds.take(validation_size)

        # cache, shuffle and prefetch
        train_ds = self.configure_for_performance(image_count, train_ds)
        val_ds = self.configure_for_performance(image_count, val_ds)

        # print the length of train and validation sets (image file)
        train_count = tf.data.experimental.cardinality(train_ds).numpy()
        validation_count = tf.data.experimental.cardinality(val_ds).numpy()
        print(f"validation_count = {validation_count}")
        print(f"train_count = {train_count}")

        # Make sure we never run out of images. We will sample each image several
        # times in order to reach the number of required samples for training and evaluation
        # https://www.tensorflow.org/api_docs/python/tf/repeat
        train_ds = train_ds.repeat()
        val_ds = val_ds.repeat()

        # TODO: should we load_image, repeat, augment image or repeat and then load and augment?

        # load the images from the path and perform data augmentation 
        # Set `num_parallel_calls` so multiple images are loaded/processed in parallel
        # You can manually call this map with one of the following specific lambdas:
        #  - self.load_image - used for debugging. Stores image path for
        #  - self.load_image_train - used for training. Extracts the image from the debug 
        #       stream and then duplicates the image into a (x,y) tuple
        train_ds = train_ds.map(lambda i: load_image_fn(i), num_parallel_calls=tf.data.AUTOTUNE)
        val_ds = val_ds.map(lambda i: load_image_fn(i), num_parallel_calls=tf.data.AUTOTUNE)

        # return the result
        results = TFPipeResult(train_ds, val_ds, image_count, validation_count, train_count)
        return results


if __name__ == "__main__":
    print("TFPipes")
    print("TensorFlow version:", tf.__version__)
    print("Eager execution:", tf.executing_eagerly())

    props = p.MTVecProps()

    pipe1 = TFPipe(props)
    # TODO: reactivate
    # number_files_1 = pipe1.copy_files()
    # pipe1.check_copies(number_files_1)
    # pipe1.check_corresponding_masks()

    # data = pipe1.load_train_data()
    # data = pipe1.load_data(["/train/good/grid"])

    # Used for training
    # data = pipe1.load_data(["/train/good/grid"], load_image_fn=pipe1.load_image_train)
    
    ## for (image1, image2) in data.train_ds.batch(2).take(5):
    # for (image1, image2) in data.train_ds.take(5):
    #     print("Image shape: ", image1.numpy().shape)
    #     print("Image shape: ", image2.numpy().shape)
    #     diff = train.DSSIM_loss(image1, image2)
    #     print("Image diff: ", diff)

    # Used for debugging data augmentation
    data = pipe1.load_data(["/train/good/grid"], load_image_fn=pipe1.load_image)
    
    print("Sampling image data:")
    for (path, c, image, new_c) in data.train_ds.take(5):
        print("\t    Image path: ", path.numpy().decode('utf-8'))
        print("\t    Image size: ", c)
        print("\t   Image shape: ", image.numpy().shape)
        print("\tOriginal shape: ", c)
        print("\t    New shape: ", new_c)

    print("Sampling training data:")
    pipe1.sample_and_save_images( "logs/train_data/", data.train_ds, number_samples=20)
    print("Sampling testing data:")
    pipe1.sample_and_save_images( "logs/eval_data/", data.val_ds, number_samples=20)

    print('... finished processing.')

