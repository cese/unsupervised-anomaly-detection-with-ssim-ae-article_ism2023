import textwrap
import inspect

# cSpell: ignore AUC, younden

class Metrics:

    def __init__(self, dataset_name, auc, younden_stat_max, younden_stat_thresh, dice_max, dice_thresh, metric_results):
      self.dataset_name = dataset_name
      self.auc = auc
      self.younden_stat_max = younden_stat_max
      self.younden_stat_thresh = younden_stat_thresh
      self.dice_max = dice_max
      self.dice_thresh = dice_thresh
      self.metric_results = metric_results

    def to_string(self):
      line_1 = f"""
      dataset:{self.dataset_name}
      AUC:{self.auc}
      YoundenStat_max:{self.younden_stat_max}
      YoundenStat_threshold:{self.younden_stat_thresh}
      nDICE_max:{self.dice_max}
      nDICE_threshold:{self.dice_thresh}"""
      line_1 = textwrap.dedent(line_1)
      # line_1 = inspect.cleandoc(line_1)
      # line_2 = ', '.join(map(str,self.loss_samples))
      # return f"{line_1}\nLoss samples:\n{line_2}\n"
      return f"{line_1}\n"

    def __str__(self):
      return self.to_string()

    def __repr__(self):
      return self.to_string()
