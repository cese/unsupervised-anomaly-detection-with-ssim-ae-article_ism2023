"""Small script that extract the MVTec data set from the cloned repository
to the same repository but in the /cache folder
"""
# cSpell: ignore tqdm, mvtec

import os
import zipfile
import tarfile
from tqdm import tqdm

import properties as dp

def list_categories(data_dir):
  """Lists the data sets for each category"""
  # for category in tqdm(os.listdir(f"{data_dir}")):
  for category in os.listdir(f"{data_dir}"):
    print(category)

def unzip_categories(source_dir, dest_dir, dry_run = False):
  """Unzips the data sets into the cache folder
  see https://note.nkmk.me/en/python-zipfile/
  see https://stackoverflow.com/questions/17217073/how-to-decompress-a-xz-file-which-has-multiple-folders-files-inside-in-a-singl
  """
  for category in tqdm(os.listdir(f"{source_dir}")):
    src = os.path.join(source_dir, category)
    print(f"Unzipping {src} to {dest_dir} (dry run = {dry_run})")
    if src.endswith("zip"):
      with zipfile.ZipFile(src) as f:
        if (not dry_run): f.extractall(dest_dir)
    else:
      with tarfile.open(src) as f:
        if (not dry_run): f.extractall(dest_dir)


if __name__ == "__main__":
  print("data: mvtec_ad")
  print(dir(dp.Properties))
  # list_categories(dp.Properties.data_dir)
  # unzip_categories(dp.Properties.data_dir, dp.Properties.data_cache, dry_run=True)
  unzip_categories(dp.Properties.data_dir, dp.Properties.data_cache)

  # https://stackoverflow.com/questions/4787413/rename-files-and-directories-add-prefix
  # for f in * ; do mv -- "$f" "PRE_$f" ; done
  # ls | xargs -I {} mv {} {}_SUF
  # find * -maxdepth 0 -exec mv {} PRE_{} \;
  # https://unix.stackexchange.com/questions/19654/how-do-i-change-the-extension-of-multiple-files
  #   https://mywiki.wooledge.org/ParsingLs
  # https://www.baeldung.com/linux/rename-files-by-removing-extension
  # mv ../mvtec_ad/cache/texture_1/ground_truth/defective/*.png ../mvtec_ad/cache/texture_1/ground_truth/defective/*_mask.png

