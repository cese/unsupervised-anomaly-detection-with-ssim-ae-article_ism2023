# cSpell: ignore tensorboard, keras, protobuf

import keras_tuner
from tensorflow import keras
import numpy as np
import numpy.typing as npt

import properties as dp


def build_model(hp):
  model = keras.Sequential()
  model.add(keras.layers.Dense(
      hp.Choice('units', [8, 16, 32]),
      activation='relu'))
  model.add(keras.layers.Dense(1, activation='relu'))
  model.compile(loss='mse')
  return model

if __name__ == "__main__":
  print("Keras Tuner Ex1")

  tune_dir = f"{dp.Properties.results_dir}/keras_tuner_ex1/"
  try:
      os.makedirs(tune_dir)
  except:
      pass

  # Keras tuner automatically add the HDParams for visualization
  # https://keras.io/guides/keras_tuner/visualize_tuning/
  # https://www.tensorflow.org/tensorboard/hyperparameter_tuning_with_hparams
  tensorboard_callback = keras.callbacks.TensorBoard(
                                  log_dir=tune_dir,
                                  #write_graph=False, # avoid large log file
                                  write_graph=True,
                                  write_images=True,
                                  histogram_freq=1,  # How often to log histogram visualizations
                                  embeddings_freq=2,  # How often to log embedding visualizations
                                  # profile_batch=(1,200), # Profile the batch(es) to sample compute characteristics. Default off   
                                  update_freq="epoch", # batch, epoch or number for unit count of metrics
                              )  # How often to write logs (default: once per epoch)

  tuner = keras_tuner.RandomSearch(
                          build_model,
                          objective='val_loss',
                          max_trials=5,
                          overwrite=True,
                          directory=tune_dir,
                          project_name="keras_tuner_ex1"
                          )

  x_train = np.random.rand(100, 10)
  y_train = np.random.rand(100, 1)
  x_validation = np.random.rand(20, 10)
  y_validation = np.random.rand(20, 1)
  tuner.search(
          x_train, y_train, 
          epochs=5, 
          validation_data=(x_validation, y_validation),
          # Use the TensorBoard callback.
          callbacks=[ tensorboard_callback ],
          )

  # https://github.com/keras-team/keras-tuner/issues/883
  # For 'keras-tuner==1.3.*' need to downgrade to 'protobuf==3.20.*'
  # Exception ignored in: <function _CheckpointRestoreCoordinatorDeleter.__del__ at 0x7f7201745b40>
  # Traceback (most recent call last):
  #   File "/home/vscode/.local/lib/python3.10/site-packages/tensorflow/python/checkpoint/checkpoint.py", line 194, in __del__
  # TypeError: 'NoneType' object is not callable
  best_model = tuner.get_best_models()[0]
  