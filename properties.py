# cSpell: ignore mvtec

import os
import os.path

class Properties:
  dataset           = None
  # original data
  data_dir          = "../mvtec_ad/data"
  # cache were archives have been decompressed
  data_cache        = "../mvtec_ad/cache"
  # experimental results
  results_dir       = "results"
  root              = None
  data_train        = "train"
  data_test         = "test"
  data_truth        = "truth"
  train_size        = 10000
  # original use 0.8*10000 (samples) = 8000. 
  # At batch size 8, 8000/8 = 1000 steps per epoch
  steps_per_epoch   = 1000  
  # At batch size 8, 2000/8 = 1000 steps per epoch
  validation_steps  = 250
  samples_per_image = 1
  validation_split  = 0.2
  batch_size        = 8
  # resize_dims       = [256, 256]
  # resize_dims       = [512, 512]
  # resize_dims       = {"carpet": [512, 512], "grid": [512, 512], "texture_1": [256,256], "texture_2": [256,256] }
  input_shape       = (128, 128, 1)
  map_samples       = dict()

  train_sets = ['carpet', 'grid', 'texture_1', 'texture_2', 'leather', 'tile', 'wood', ] # 'zipper'
  test_sets = train_sets

  def resize_dims(self, file_name: str) -> tuple[int, int]:
    # img_resized_size = (0,0)
    if self.contains(file_name, "texture_1") or \
       self.contains(file_name, "texture_2"):
        #img_in_size = (512, 512)
        img_resized_size = (256, 256)
        #crop_size = (128, 128)
        #step = 16
    elif self.contains(file_name, "grid") or \
         self.contains(file_name, "carpet"):
        #img_in_size = (1024, 1024)
        img_resized_size = (512, 512)
        #crop_size = (128, 128)
        #step = 32
    else:
        img_resized_size = (0, 0)
    return img_resized_size

  def contains(self, name: str, has: str) -> bool:
    return name.find(has) >= 0

  def contains_all(self, name: str, has: list[str]) -> tuple[bool, str]:
    for h in has:
      if not self.contains(name, h):
        return (False, h)
    return (True, None)

  def contains_any(self, name: str, has: list[str]) -> tuple[bool, str]:
    for h in has:
      if self.contains(name, h):
        return (True, h)
    return (False, None)
  
  def is_training_sample(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('train: file name is empty')
    return (False, None)

  def is_test_sample(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('test: file name is empty')
    return (False, None)

  def is_ground_truth(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('ground truth: file name is empty')
    return (False, None)


class MTVecProps(Properties):
  # def __init__(self):
  #   pass

  dataset           = "mvtec_ad"
  # original data
  data_dir          = "../mvtec_ad/data"
  # cache were archives have been decompressed
  data_cache        = "../mvtec_ad/cache"
  # experimental results
  results_dir       = "results"
  root              = "./data"
  train_size        = 10000
  # original use 0.8*10000 (samples) = 8000. 
  # At batch size 8, 8000/8 = 1000 steps per epoch
  steps_per_epoch   = 1000  
  # At batch size 8, 2000/8 = 1000 steps per epoch
  validation_steps  = 250
  samples_per_image = 3
  validation_split  = 0.2
  batch_size        = 8
  # resize_dims       = [256, 256]
  # resize_dims       = [512, 512]
  # resize_dims       = {"carpet": [512, 512], "grid": [512, 512], "texture_1": [256,256], "texture_2": [256,256] }
  input_shape       = (128, 128, 1)
  map_samples = {
      "train"        : os.path.join(root, Properties.data_train),
      "test"         : os.path.join(root, Properties.data_test),
      "ground_truth" : os.path.join(root, Properties.data_truth)
      }

  train_sets = ['carpet', 'grid', 'texture_1', 'texture_2', 'leather', 'tile', 'wood', ] # 'zipper'
  test_sets = train_sets

  def is_training_sample(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('train: file name is empty')
    (r, v) = self.contains_any(file_name, self.train_sets)
    return (self.contains(file_name, "train") and r, v)

  def is_test_sample(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('test: file name is empty')
    (r, v) = self.contains_any(file_name, self.test_sets)
    return (self.contains(file_name, "test") and r, v)

  def is_ground_truth(self, file_name: str) -> tuple[bool, str]:
    if file_name == "": 
      raise RuntimeError('ground truth: file name is empty')
    (r, v) = self.contains_any(file_name, self.test_sets)
    return (self.contains(file_name, "ground_truth") and r, v)
