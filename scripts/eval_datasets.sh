#!/bin/bash

SCRIPT=AE_evaluation.py
FILE_NAME="$(basename "$1")"
echo "Executing script: $FILE_NAME"

LIB=$(dirname "$1")/dataset_funcs.sh
# echo "Loading lib $LIB"
source $LIB

# If we are in the scripts directory, go to the root
change_dir

# Get the dataset name from the script's file name (last word after '_')
DATASET=$(get_dataset $FILE_NAME 5)
echo "Dataset: $DATASET"

# https://stackoverflow.com/questions/3811345/how-to-pass-all-arguments-passed-to-my-bash-script-to-a-function-of-mine
# python $SCRIPT "$@"
# python $SCRIPT "${@:2}" 

nohup python $SCRIPT "${@:2}"\
 2> results/error_eval_$DATASET.txt 1> results/output_eval_$DATASET.txt &
