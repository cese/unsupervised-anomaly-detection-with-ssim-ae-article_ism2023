#!/bin/bash

# $1 - dataset name
# $2 - keras tuner slave number (part of its ID)

SCRIPT=AE_tune.py 
DATASET=$1
LATENT_DIM=100
BATCH_SIZE=8
TRAINING_LOSS="ssim"
USE_TF_DATASET=true
N_TRAIN=10000
MAX_EPOCHS=200
N_WORKERS=16

# Execute from the root of the project
#cd ..
    # default: 
    # --no-load_model\
    # --random_crop\
    # --no-use_tf_dataset
    # --n_train=10000\
    # --max_epochs=200\
    # --n_workers=16\
    # Other options
    # --load_model\  
    # --no-random_crop\
    # --use_tf_dataset\


echo "Tuner ID: $KERASTUNER_TUNER_ID"

# default value for $1
ID=${2:0}

ERROR="model_weights/$DATASET/error_$ID.txt"
echo "ERROR=$ERROR"
OUT="model_weights/$DATASET/output_$ID.txt"
echo "OUT=$OUT"


nohup python $SCRIPT\
    --dataset_name $DATASET\
    --latent_dim $LATENT_DIM\
    --batch_size $BATCH_SIZE\
    --training_loss $TRAINING_LOSS\
    --no-load_model\
    --random_crop\
    --use_tf_dataset\
    --n_train=$N_TRAIN\
    --max_epochs=$MAX_EPOCHS\
    --n_workers=$N_WORKERS\
 2> $ERROR 1> $OUT &