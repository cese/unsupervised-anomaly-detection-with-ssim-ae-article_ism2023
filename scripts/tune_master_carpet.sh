#!/bin/bash

# https://keras.io/guides/keras_tuner/distributed_tuning/

export KERASTUNER_TUNER_ID="chief"
export KERASTUNER_ORACLE_IP="127.0.0.1"
export KERASTUNER_ORACLE_PORT="8000"

# The dataset name is set by this script's filename
# The name in $0 is scanned from position 12 until 
# the extension to extract the dataset name

./tune_filename.sh "$0" 12 "master"