#!/bin/bash

ROOT_PATH="log"
DATASET="all"

# ./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "$DATASET/original" "$DATASET/regression_2b"
# ./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "$DATASET/regression_2b"
# ./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "texture_1/regression_2b" "texture_2/regression_2b" "carpet/regression_2b" "grid/regression_2b"
./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "texture_1/regression_1" "texture_2/regression_1" "carpet/regression_1" "grid/regression_1"
