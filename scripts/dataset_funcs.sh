#!/bin/bash

function change_dir {  
  PWD=$(pwd)
  echo "PWD: $PWD"
  if [[ $PWD = *scripts* ]]; then 
    echo "Changing to parent dir"; 
    cd ..
    echo "New PWD: $(pwd)"
  fi
}

# https://www.linuxjournal.com/content/return-values-bash-functions
# https://stackoverflow.com/questions/10822790/can-i-call-a-function-of-a-shell-script-from-another-shell-script
function get_dataset {  
  FILE_NAME=$1
  EXTENSION="${FILE_NAME##*.}"
  FILENAME="${FILE_NAME%.*}"

  # https://linuxgazette.net/18/bash.html
  # https://www.tldp.org/LDP/abs/html/string-manipulation.html
  # https://www.baeldung.com/linux/bash-string-manipulation#2-substrings 
  # DATASET=${FILENAME:6}
  DATASET=${FILENAME:$2}
  echo "$DATASET"
}
