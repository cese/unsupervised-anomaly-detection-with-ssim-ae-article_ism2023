#!/bin/bash

# We need to rename the texture defect masks so that they are in line with 
# the the MVTec AD dataset so that the Python evaluation can assume the 
# same name structures.
#
# Run this script after the ../data_mvtec_ad.py Pythin script and before the
# AE_evaluation.py Python script

echo "Renaming texture_1 masks"

#for f in *.txt; do
#for f in ../../mvtec_ad/cache/texture_1/ground_truth/defective/*.png; do
#for f in ../../mvtec_ad/cache/texture_1/ground_truth/*/*.png; do
for f in ../../mvtec_ad/cache/texture_1/ground_truth/*/*.png; do
  new_name="${f%.png}_mask.png"
  echo "Renaming $f to $new_name"
  # mv -- "$f" "${f%.png}_mask.png"
  mv -- "$f" $new_name
done

echo "Renaming texture_2 masks"

for f in ../../mvtec_ad/cache/texture_2/ground_truth/*/*.png; do
  new_name="${f%.png}_mask.png"
  echo "Renaming $f to $new_name"
  mv -- "$f" $new_name
done
