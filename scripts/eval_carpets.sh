#!/bin/bash

ROOT_PATH="log"
DATASET="carpet"

# ./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "$DATASET/original" "$DATASET/regression_2b"
./eval_datasets.sh $0 --metrics_file $DATASET --path_to_load $ROOT_PATH --datasets_names "$DATASET/original" "$DATASET/regression_1"
