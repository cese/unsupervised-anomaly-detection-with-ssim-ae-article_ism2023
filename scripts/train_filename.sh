#!/bin/bash

FILE_NAME="$(basename "$1")"
echo "Executing script: $FILE_NAME"

LIB=$(dirname "$1")/dataset_funcs.sh
# echo "Loading lib $LIB"
source $LIB

# If we are in the scripts directory, go to the root
change_dir

# Get the dataset name from the script's file name (last word after '_')
DATASET=$(get_dataset $FILE_NAME 6)
echo "Dataset: $DATASET"

./scripts/train_dataset.sh $DATASET