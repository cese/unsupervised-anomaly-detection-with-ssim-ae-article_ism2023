#!/bin/bash

SCRIPT=AE_training.py
DATASET=$1
LATENT_DIM=100
BATCH_SIZE=8
TRAINING_LOSS="ssim"

# Execute from the root of the project
#cd ..
    # default: 
    # --no-load_model\
    # --random_crop\
    # Other options
    # --load_model\  
    # --no-random_crop\
    # --use_tf_dataset
    # --no-use_tf_dataset

nohup python $SCRIPT\
    --dataset_name $DATASET\
    --latent_dim $LATENT_DIM\
    --batch_size $BATCH_SIZE\
    --training_loss $TRAINING_LOSS\
    --no-load_model\
    --random_crop\
    --use_tf_dataset\
 2> model_weights/$DATASET/error.txt 1> model_weights/$DATASET/output.txt &