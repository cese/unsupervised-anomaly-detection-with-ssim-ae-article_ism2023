#!/bin/bash

# $1 - filename that invoked this script. Used to extract the databse name
# $2 - number of characters to strip from the filename to get the datasae name
# $3 - ID that identifies a Keras tuner slave (default to 0)

FILE_NAME="$(basename "$1")"
echo "Executing script: $FILE_NAME"

LIB=$(dirname "$1")/dataset_funcs.sh
# echo "Loading lib $LIB"
source $LIB

# If we are in the scripts directory, go to the root
change_dir

# Get the dataset name from the script's file name (last word after '_')
DATASET=$(get_dataset $FILE_NAME $2)
echo "Dataset: $DATASET"

echo "Tuner ID: $KERASTUNER_TUNER_ID"

./scripts/tune_dataset.sh $DATASET $3