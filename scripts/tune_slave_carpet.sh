#!/bin/bash

# https://keras.io/guides/keras_tuner/distributed_tuning/

# export KERASTUNER_TUNER_ID="$0_0"
export KERASTUNER_ORACLE_IP="127.0.0.1"
export KERASTUNER_ORACLE_PORT="8000"

validateIntegers() {

    if ! [[ "$1" =~ ^[0-9]+$ ]]; then
        return 1 # failure
    fi
    return 0 #success

}

die() { echo "$*" 1>&2 ; exit 1; }

# default value for $1
MULTI=${1:8}

# Check if $1 is defined otherwise print usage
# Check that it is an integer otherwise fail with error message
: ${1?"forgot to supply an argument $0 Usage: $0 'number of tuners'"} && validateIntegers $1 || die "Must supply an integer argument: number of tuners"
if [ "$1" -gt "-1" ]
  then
    MULTI=$1
    echo "Number of concurrent tuners = $MULTI"
  else
    echo "No numeric argument supplied to set number of concurrent tuners. Setting default $MULTI"
fi

# https://stackoverflow.com/questions/169511/how-do-i-iterate-over-a-range-of-numbers-defined-by-variables-in-bash
# Cannot use {1.."$MULTI"}
# for value in {1.."$MULTI"}
for ((value=1;value<=$MULTI;value++))
do
  export KERASTUNER_TUNER_ID="$0:$value"
  echo "Starting $KERASTUNER_TUNER_ID with ID = $value"
  ./tune_filename.sh "$0" 11 $value
done

