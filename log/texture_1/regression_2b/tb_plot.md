<!-- Steps is the number of batches processed. Steps per epoch = data size / batch size -->
epochs: 40 (41K steps)
time: 5.835 hours (min 5.548)
loss smoothed: 0.1622 (min 0.1619)
loss: 0.1627 (min 0.1618)

