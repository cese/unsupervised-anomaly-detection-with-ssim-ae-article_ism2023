
# cSpell: ignore tf, tfpipes, tfp, imread, ssim
# cSpell: ignore matplotlib, pyplot, plt

# https://stackoverflow.com/questions/714063/importing-modules-from-parent-folder

import os
import sys
import inspect
import cv2

import os.path
import glob
import itertools as it
import matplotlib.pyplot as plt
from typing import Tuple
from typing import NamedTuple
#from typing import Sequence, Tuple, Union, SupportsIndex
from skimage.metrics import structural_similarity as ssim
import numpy as np


# if _file__ is not defined
# current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
# if there is another module with the name, make sue this one comes first
# sys.path.insert(0, parent_dir) 

# Shorter
sys.path.insert(0, os.path.join(sys.path[0], '..'))

import properties as dp
import tfpipes as tfp


def read_image(file: str, resized_size: int = None):
  image = cv2.imread( file, cv2.IMREAD_GRAYSCALE)
  image = image.astype("float32") / 255.0
  if resized_size != None:
    image = cv2.resize(image, resized_size)
  return image

def compare_images(reference: cv2.Mat, other: cv2.Mat, win_size: int = 11):
    _, S = ssim( 
              im1=reference, 
              im2=other, 
              data_range=1, 
              gradient=False, 
              full=True, 
              multichannel=False, 
              win_size=win_size )
    loss = (1 - S)/2
    return loss

class Sample(NamedTuple):
    max: float
    min: float
    mean: float
    median: float
    std: float
    sum: float
    base_file: str
    other_file: str

EmptySample = Sample(0,0,0,0,0,0,"","")
fields = list(EmptySample._fields)

def csv_header(t: NamedTuple) -> str:
  fields = list(t._fields)
  return ', '.join(fields) + "\n"
   
def csv_field(x) -> str :
  if type(x) == str:
     return f'"{x}"'
  else:
     return str(x)

def csv_values(t: NamedTuple) -> str:
  dictionary = t._asdict()
  # values = map( lambda f: str(dictionary[f]) , fields)
  values = map( lambda f: csv_field(dictionary[f]) , fields)
  return ', '.join(values) + "\n"

def debug_image(m: cv2.Mat, reference_file: str, other_file: str) -> Sample :
    max_m = m.max()
    min_m = m.min()
    mean_m = m.mean()
    median_m = np.median(m)
    std_m = m.std()
    sum_m = m.sum()
    return Sample(
              max        = max_m,
              min        = min_m,
              mean       = mean_m,
              median     = median_m,
              std        = std_m,
              sum        = sum_m,
              base_file  = reference_file,
              other_file = other_file
              )
   

def conv(crop: tuple[int, int]):
   pass

if __name__ == "__main__":

  datasets_names = ["carpet"]
  # path_to_train_set = f"{dp.Properties.data_cache}/{dataset_name}/train/"
  path_to_train_set = "./data/test/good"

  print(f"path_to_train_set = {path_to_train_set}")

  props = dp.MTVecProps()
  pipe = tfp.TFPipe(props)
  files = pipe.images_only(path_to_train_set, pipe.supported, recursive=True)

  # filter out the files if required
  if datasets_names:
      print(f"datasets_names = {datasets_names}")
      files = filter( lambda f: props.contains_any(f, datasets_names)[0], files )

  files = list(files)
  files.sort()

  metrics_file = f"image_metrics.csv"
  print(f"metrics_file: {metrics_file}")


  # temp_dir = "/tmp/ssim_s"
  temp_dir = "./results/ssim_s"
  temp_dir = os.path.join(dp.Properties.results_dir, "ssim_s")
  # os.rmdir(temp_dir)
  if not os.path.exists(temp_dir):
      os.mkdir(temp_dir)

  files_data = list( map(lambda d: read_image(d), files) )

  reference = 0
  reference_file = files_data[reference]

  # Interactively showing images
  # color = cv2.imread( files[reference])
  # gray = cv2.cvtColor(color, cv2.COLOR_BGR2GRAY)
  # plt.imshow(color)
  # plt.waitforbuttonpress()
  #
  # CV2 Does not work headless
  # https://stackoverflow.com/questions/40207011/opencv-not-working-properly-with-python-on-linux-with-anaconda-getting-error-th/51013434#51013434
  # print(cv2.getBuildInformation())
  # cv2.imshow('Original image',color)
  # cv2.imshow('Gray image', gray)
  # 
  # cv2.waitKey(0)
  # cv2.destroyAllWindows()

  # metrics_file = os.path.join(dp.Properties.results_dir, metrics_file)
  metrics_file = os.path.join(temp_dir, metrics_file)
  with open(metrics_file, 'w') as f:
    f.write(csv_header( EmptySample ) )

    for i in range(len(files)):
      print(f"Comparing reference={files[reference]}, other={files[i]}")
      o = files_data[i]
      loss = compare_images(reference=reference_file, other=o, win_size=11)
      plt.clf()
      plt.imshow(loss, vmax=1, cmap="jet")
      plt.colorbar()

      save_dir = os.path.join(temp_dir, f"{i}")
      if not os.path.exists(save_dir):
          os.mkdir(save_dir)
      base = os.path.basename(files[i])
      file, ext = os.path.splitext(base)
      save = f"{file}_loss{ext}"
      save = os.path.join(temp_dir, f"{i}", save)
      print(f"Saving heatmap to: {save}")
      plt.savefig(save)

      base = os.path.basename(files[reference])
      save = os.path.join(temp_dir, f"{i}", base)
      print(f"Saving reference to: {save}")
      # cv2.imwrite(save, reference_file*255)
      cv2.imwrite(save, reference_file*255)

      base = os.path.basename(files[i])
      save = os.path.join(temp_dir, f"{i}", base)
      print(f"Saving original to: {save}")
      cv2.imwrite(save, o*255)

      d = debug_image(loss, reference_file=files[reference], other_file=files[i])
      v = csv_values(d)
      f.write( v )

  f.close()
  print(f'Metrics saved to {metrics_file}')
  print("Finished")
